# Introduction

ROMBA (ROM Board for Amstrad) is a Flash ROM cartridge with additional DMA capabilities and an additional digital audio channel. It is plugged to the expansion port of Amstrad CPC computers either directly or through an MX-4 board.

![EDGE connector version](doc/romba-ra1-edge.png)

![MX-4 version](doc/romba-ra1-m4.png)

## Features

* Up to 64 megabits Flash ROM capacity.
* Compatible with Amstrad CPC Romboxes (up to 256 slots).
* Rombox expansion for up to 512 slots (to reach 64 Megabits).
* Slot 7 is mapped to the lower ROM (so the CPC can boot directly from the cart without entering BASIC).
* One additional digital audio channel, using 8-bit samples and a programmable sampling frequency going from 3.9 kHz to 50 kHz and beyond!
* 1 DMA channel that can be used to copy data from ROM to internal RAM (1 byte per microsecond) or to play digital audio samples without CPU intervention.
* Flash ROM can be written to from the CPC itself with some restrictions (useful for example to save games and scores).
* ROMs are programmed using a fast USB programmer, included in this project.

The cartridge hardware design allows for some more features that are currently unsupported because there is not enough space in the cartridge CPLD for them, but could be enabled if using a bigger CPLD, or if removing some functionality to implement other:

* NMI is connected to the CPLD.
* 4 additional 1-bit digital audio channels are connected to the CPLD. These could be used for 1-bit PCM playback and/or for generating square waves for instruments.
* There is an RGB LED connected to the CPLD, that currently is used only for debugging. You could maybe use it for other purposes, but that would require some way of making the light go out of the case. Currently it is not located in the best place it could, at least if you plan to plug the cart directly in the expansion port.

There are also some more things I would like to check, but had not the time to dig into:

* I have been suggested by TotO that maybe during HBLANK the GA does not perform reads from RAM. If I can confirm this, DMA speed could be greatly increased during HBLANK time interval.
* Currently DMA only works from ROMBA ROM to CPC internal RAM or from ROMBA ROM to the DAC (for audio). It should be possible to DMA data from/to CPC RAM, but speed would be halved (1 byte each 2 microseconds instead of 1 byte per microsecond) and also banking should be taken care externally (the signals needed to address the different RAM banks are not connected to the expansion port).

I am not planning on implementing these features. Anyway for version 1.0, CPLD usage is 100%, so even if I wanted to implement anything else, it will not fit unless switching to a bigger CPLD or removing some of the currently implemented features. If you want to contribute some of them, I can consider merging it to a separate branch, but I want the `master` branch to fit inside a budget CPLD.

## Repository organization

This repository contains several subprojects:

* `doc`: schematics of the cartridge and programmer in PDF format.
* `sch/lib`: additional components required by schematics, not included in standard KiCAD libraries.
* `sch/romba`: cartridge PCB design files, including schematics, PCB and Gerber files. You will need KiCAD to open the design files.
* `sch/romprog`: programmer PCB design files, including schematics, PCB and Gerber files. You will need KiCAD to open the design files.
* `sch/exp-shell`: FreeCAD design files for the cartridge enclosure, along with the STL files for 3D printing.
* `sch/prog-shell`: FreeCAD design files for the programmer enclosure, along with the STL files for 3D printing.
* `src/romba-cli`: sources for the programming software used to write ROM files to the cartridge.
* `src/romba-fw`: sources for the firmware running in the cartridge programmer.
* `src/romba-bl`: bootloader used in the programmer to allow upgrading the firmware via USB. Note using this bootloader is not required.
* `src/hdl/romba`: HDL sources for the CPLD in the cartridge, along with the files required to run a simulation using [GHDL](http://ghdl.free.fr/) and GTKWave.
* `src/hdl/lromba`: project used to synthesize and burn the HDL into the CPLD, using [Lattice Diamond](https://www.latticesemi.com/latticediamond) suite. Note the sources in this path symlink the code in `src/hdl/romba` so there are not two independent copies of the same sources.

## 3rd party sources

This repository also includes the following 3rd party Open Source software:

* `romba-bl` is adapted from the DFU bootloader example included in the LUFA Library (MIT license).
* `romba-fw` uses some modified code from the LUFA Library bulk transfer example (MIT license).

# Programming for ROMBA cartridges

There is a separate guide dealing with this topic. Please read [PROGRAMMING.md file](PROGRAMMING.md) if you are an Amstrad CPC developer and you want to use this cart.

# Building ROMBA

There are many subprojects here and you might be wondering from where to start, right? You will have to use them as instructed in this section to build two things: the ROMBA programmer (used to burn ROMs from your PC) and the ROMBA cartridge itself.

## Building the programmer

To build the ROMBA programmer needed to flash ROMs to ROMBA cartridges, you have to:

1. Get the PCB in `sch/romprog` fabricated.
2. (OPTIONAL) 3D-print the programmer enclosure and assemble the PCB inside it.
3. (OPTIONAL) Build and flash the `romba-bl` bootloader. Read the subproject [README.md file](src/romba-bl/README.md) for more details.
4. Build and flash the `romba-fw` firmware. Read the subproject [README.md file](src/romba-fw/README.md) for more details.

The completely assembled programmer (with a cartridge plugged at the end) looks like this:

![ROMBA programmer](doc/romba-prog.jpg)

## Building the cartridge

To build a ROMBA cartridge, follow these steps:

1. Get the PCB in `sch-romba` fabricated. Note that you will have to choose a mount option: either an edge connector or a DIL one for MX-4 boards.
2. Build and program the CPLD bitfile from the sources in `src/hdl/lromba`. Read the subproject [README.md file](src/hdl/README.md) file for more details.
3. (OPTIONAL) 3D-print the cartridge enclosure in `sch/exp-shell` and assemble the PCB inside it. Note that this case is not prepared for the MX-4 connector option, it will only fit properly when using the edge connector.
4. Compile the programmer software in `src/romba-cli`. Use the built `romba` program to burn ROMs to the cartridge. Read the subproject [README.md file](src/romba-cli/README.md) file for more details.

The completely assembled cartridge using an edge connector and with the 3D-printed case (printed in ABS and smoothed with acetone) looks like this:

![ROMBA programmer](doc/romba-cart.jpg)

# Author

All code and design files in this repository (with the exception of the 3rd party sources listed above) have been created by Jesús Alonso (doragasu).

# License

The files in this repository are provided with NO WARRANTY of any type. The licenses used for each subcomponent are as follows:

* `sch/lib`: [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
* `sch/romba`, `sch/romprog`, `sch/exp-shell` and `sch/prog-shell`: [CERN-OHL-S](https://ohwr.org/cern_ohl_s_v2.txt) version 2.
* `src/romba-cli`, `src/romba-fw`, `src/romba-bl`, `src/hdl/romba` and `src/hdl/lromba`: [GPL version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).


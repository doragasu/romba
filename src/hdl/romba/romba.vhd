-- romba.vhd: ROMBA HDL
-- doragasu, 2017
-- License: GPLv3

-- Documentation read during development:
-- * http://cpctech.cpc-live.com/docs/iopord.html
-- * http://www.cpcwiki.eu/index.php/I/O_Port_Summary

-- Assumptions (most of them based on my understanding of
-- http://www.cpcwiki.eu/index.php/Connector:Expansion_port, and on the fact
-- that #RD, #WR and #IORQ are directly driven by the Z80):
-- - For ROM reads, [#MREQ = 0], #ROMEN = 0, #RD = 0
-- - ROM writes (#MREQ = 0, #ROMEN= 0, #WR = 0) are not generated.
-- - For RAM reads [#MREQ = 0], #RAMRD = 0, #RD --!TODO Controlling addr, n_mreq, n_iorq, n_rd and n_wr transceiver enable
-- might allow to separate domains, and read from flash while the CPU is
-- working. This could be useful to avoid wait states while the DMA
-- controller is reading from cartridge flash!

-- * For RAM writes, #MREQ = 0, #WR = 0.
-- * For I/O reads, #IORQ = 0, #RD = 0.
-- * For I/O writes, #IORQ = 0, #WR = 0.


-- IO Mapped registers:
-- * 0xFC00 | SRCL | Source low byte
-- * 0xFC01 | SRCH | Source high byte
-- * 0xFC02 | SRCU | Source upper byte
-- * 0xFC04 | DSTL | Destination low byte
-- * 0xFC05 | DSTH | Destination high byte
-- * 0xFC06 | LENL | Transfer length, low byte
-- * 0xFC07 | LENH | Transfer length, high byte
-- * 0xFC08 | CTRL | DMA Control
-- * 0xFC09 | DDIV | DMA DAC clock divisor (8 bit)
-- * 0xFC0A | DAC  | DAC output register
-- * 0xFC0B | RSEH | ROM select high bit
-- * 0xFC0C | CONF | Configuration register
-- * 0xDCXX ~ 0xDFXX | RSEL | ROM select, low byte, compatible with romboxes
--
-- CTRL register at 0xFC08:
-- * B0 | START  | Write 1 to start DMA.
-- * B1 | SRCINC | ROM to RAM source autoincrement (when 1)
-- * B2 | DSTINC | ROM to RAM destination autoincrement (when 1)
-- * B3 ~ B5     | RESERVED
-- * B6 | FADR   | Full addresing:
--   - 0: Use current 16 KiB bank.
--   - 1: Use full 23 bit addressing.
-- * B7 | MODE   | DMA Mode:
--   - 0: ROM to RAM.
--   - 1: ROM to DAC.
--
-- DMA MODES:
-- * MODE 0: ROM to RAM: Each microsecond, a byte is read from ROM and
--           and copied to RAM. The RAM write is performed when READY
--           signal is lowered.
-- * MODE 1: ROM to DAC. At regular intervals (depending on DDIV), a byte
--           is read from ROM and written to DAC register. The frequency Fo
--           of writes (in kHz) is calculated as Fo = 1000 / (DDIV + 1)
--
-- CONF register at 0xFC0C:
-- * B0 | TRANSPARENT: When set to 1, transparent ROM addressing is enabled.
--        This allows reading from and writing to ROM slot 7, that is normally
--        not allowed to avoid clash with CPC 6128 internal DOS ROM.
-- * B1 | WRITE_EN: Write enable for the flash chip. When set to '1', writes
--        in the $C000 ~ $FFFF range will be sent to the flash chip. Note that
--        these writes will also go to the RAM in this zone!

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
-- Uses 69 pins
-- ROM mapped at $C000 ~ $FFFF

ENTITY romba IS
    PORT (
            -- Z80 CPU BUSES --
            addr    : INOUT  STD_LOGIC_VECTOR(15 DOWNTO 0); --CPU A15 to A0
            data    : INOUT  STD_LOGIC_VECTOR(7 DOWNTO 0);  --CPU D7 to D0
            n_iorq  : IN  STD_LOGIC;                    --CPU IO request
            n_mreq  : INOUT  STD_LOGIC;                 --Memory request
            n_rd    : IN  STD_LOGIC;                    --CPU read
            n_wr    : INOUT  STD_LOGIC;                 --CPU write
            n_m1    : IN  STD_ULOGIC;                   --Z80 M1
            n_rfsh  : IN  STD_ULOGIC;                   --Z80 Refresh
            n_busrq : OUT STD_ULOGIC;                   --CPU BUS request
            n_busak : IN  STD_ULOGIC;                   --CPU BUS acknowledge
            n_nmi   : OUT STD_ULOGIC;                   --Non maskarable int
            n_rset  : IN  STD_ULOGIC;                   --Reset
            clk4    : IN  STD_ULOGIC;                   --Z80 4MHz clock

            --GA signals
            n_ramrd : IN  STD_ULOGIC;                   --RAM read active
            n_romen : IN  STD_ULOGIC;                   --GA ROM enable
            ready   : IN  STD_ULOGIC;                   --Ready signal
            ramdis  : OUT STD_ULOGIC;                   --Disable internal RAM
            romdis  : OUT STD_ULOGIC;                   --Internal ROM disable

            -- Transceiver control --
            t_dir   : OUT STD_ULOGIC;                   --Direction
            tn_oe   : OUT STD_ULOGIC;                   --Output enable
            n_busbr : OUT STD_ULOGIC;                   --Bridge CPC & cart buses
            dma_bm  : OUT STD_ULOGIC;                   --DMA Bus Master

            -- Flash chip interface --
            fn_addr: OUT STD_ULOGIC_VECTOR(22 DOWNTO 14);--Flash higher lines
            fn_ce  : OUT STD_ULOGIC;                     --Chip enable
            fn_oe  : OUT STD_ULOGIC;                     --Output enable
            fn_we  : OUT STD_ULOGIC;                     --Write enable

            -- Sound generation
            daco   : OUT STD_ULOGIC_VECTOR(7 DOWNTO 0);  --8 bit dac output
            sqw    : OUT STD_ULOGIC_VECTOR(3 DOWNTO 0);  --Pulse outputs

            -- OTHER --
            led    : OUT STD_ULOGIC_VECTOR(2 DOWNTO 0)   --RGB LED.
         );
END romba;

ARCHITECTURE behavioral OF romba IS
    -- DMA registers
    SIGNAL src      : UNSIGNED(22 DOWNTO 0);
    SIGNAL dst      : UNSIGNED(15 DOWNTO 0);
    SIGNAL len      : UNSIGNED(15 DOWNTO 0);
    SIGNAL ctrl     : STD_ULOGIC_VECTOR(2 DOWNTO 1);
    SIGNAL dac      : STD_ULOGIC_VECTOR(7 DOWNTO 0);
    -- Full addressing flag
    SIGNAL fadr     : STD_ULOGIC;
    -- ROM bank register
    SIGNAL rom_bank : STD_ULOGIC_VECTOR(8 DOWNTO 0);
    -- Internal DMA scratch register
    SIGNAL scratch  : STD_ULOGIC_VECTOR(7 DOWNTO 0);
    -- Internal divisor counter for DMA to DAC
    SIGNAL divc     : UNSIGNED(9 DOWNTO 0);
    -- Reset value for divisor counter
    SIGNAL divrst   : UNSIGNED(7 DOWNTO 0);
    -- IO write
    SIGNAL iowr     : STD_ULOGIC;
    -- ROM read
    SIGNAL romrd    : STD_ULOGIC;
    -- Flash write
    SIGNAL flashwr  : STD_ULOGIC;
    -- Register names
    TYPE reg_name IS (r_srcl, r_srch, r_srcu, r_dstl, r_dsth, r_lenl,
    r_lenh, r_ctrl, r_div,  r_dac,  r_roms, r_romh, r_conf, r_none);
    SIGNAL reg_sel  : reg_name;
    ATTRIBUTE syn_romstyle : STRING;
    ATTRIBUTE syn_romstyle OF reg_sel : SIGNAL IS "block_rom";
    -- DMA machine states:
    -- * dma_idle: DMA stopped
    -- * dma_req:  Bus requested, waiting for n_busack
    -- * dma_sync: Waiting to sync with GA
    -- * dma_rd:   DMA read from ROM
    -- * dma_rdy:  Waiting for GA to set READY signal
    -- * dma_wr:   DMA write to internal CPC RAM
    -- * dma_ard:  Read audio sample from ROM
    -- * dma_awr:  DMA write audio sample to DAC
    -- * dma_atim: DMA waiting for audio timer
    TYPE dma_stat IS (dma_idle, dma_req, dma_sync, dma_rd, dma_rdy, dma_wr,
    dma_await, dma_ard, dma_awr, dma_atim);
    SIGNAL dma      : dma_stat;
    -- Input for the register multiplexer
    SIGNAL reg_mux_in : STD_LOGIC_VECTOR(9 DOWNTO 0);
    -- When '1', do not skip bank 7
    SIGNAL transparent : STD_ULOGIC;
    SIGNAL wr_en : STD_ULOGIC;

    -- True when low ROM is being read
    SIGNAL lorom : STD_ULOGIC;
    -- True when slot 7 is being accessed
    SIGNAL osrom : STD_ULOGIC;

BEGIN
    reg_mux_in <= addr(15 DOWNTO 10) & addr(3 DOWNTO 0);
    WITH reg_mux_in SELECT
        reg_sel <= r_srcl WHEN x"FC" & "00", -- FC00
                   r_srch WHEN x"FC" & "01", -- FC01
                   r_srcu WHEN x"FC" & "10", -- FC02
                   r_dstl WHEN x"FD" & "00", -- FC04
                   r_dsth WHEN x"FD" & "01", -- FC05
                   r_lenl WHEN x"FD" & "10", -- FC06
                   r_lenh WHEN x"FD" & "11", -- FC07
                   r_ctrl WHEN x"FE" & "00", -- FC08
                   r_div  WHEN x"FE" & "01", -- FC09
                   r_dac  WHEN x"FE" & "10", -- FC0A
                   r_romh WHEN x"FE" & "11", -- FC0B
                   r_conf WHEN x"FF" & "00", -- FC0C
                   -- Dunno why, don't cares '-' like below do not work on GHDL.
                   --                 r_roms WHEN "110111" & "----",
                   r_roms WHEN x"DC" & "00", -- DF00 and friends
                   r_roms WHEN x"DC" & "01", -- (1101 11XX XXXX XXXX range)
                   r_roms WHEN x"DC" & "10",
                   r_roms WHEN x"DC" & "11",
                   r_roms WHEN x"DD" & "00",
                   r_roms WHEN x"DD" & "01",
                   r_roms WHEN x"DD" & "10",
                   r_roms WHEN x"DD" & "11",
                   r_roms WHEN x"DE" & "00",
                   r_roms WHEN x"DE" & "01",
                   r_roms WHEN x"DE" & "10",
                   r_roms WHEN x"DE" & "11",
                   r_roms WHEN x"DF" & "00",
                   r_roms WHEN x"DF" & "01",
                   r_roms WHEN x"DF" & "10",
                   r_roms WHEN x"DF" & "11",
                   r_none WHEN OTHERS;

    iowr <= NOT n_iorq AND NOT n_wr AND n_m1;
    -- DOS ROM must be mapped instead of cart when slot 7 or 0x107 is selected
    osrom <= '1' WHEN transparent = '0' AND rom_bank(7 DOWNTO 0) = x"07" ELSE '0';
    -- If machine wants to read from ROM and A15 (or A14) is not set,
    -- then we are reading from lower ROM
    -- NOTE: n_romen = (LROMEN + A15 + A14)*(HROMEN + #A15 + #A14) + n_mreq + n_rd
    lorom <= NOT n_romen AND NOT addr(15);
    -- Reads are performed when reading from lower ROM or reading from upper
    -- ROM and not reading from the DOS ROM
    romrd <= lorom OR (NOT n_romen AND NOT osrom);
    -- Flash writes are mapped to memory requests on the upper ROM range
    flashwr <= wr_en AND NOT n_mreq AND NOT n_wr AND addr(15) AND addr(14);

    -- Register bank
    dma_ctrl : PROCESS(clk4,n_rset)
    BEGIN
        IF n_rset = '0' THEN
            -- Reset sets ROM to 0, DMA to idle and DAC to medium point
            dma <= dma_idle;
            rom_bank <= (OTHERS => '0');
            dac <= x"80";   -- Center point
            len <= (OTHERS => '0');
            src <= (OTHERS => '0');
            dst <= (OTHERS => '0');
            ctrl <= (OTHERS => '0');
            fadr <= '0';
            transparent <= '0';
            wr_en <= '0';
        ELSIF RISING_EDGE(clk4) THEN
            -- Check if writing to IO mapped register
            IF iowr = '1' THEN
                CASE reg_sel IS
                    WHEN r_srcl =>
                        src(7 DOWNTO 0) <= UNSIGNED(data);
                    WHEN r_srch =>
                        src(15 DOWNTO 8) <= UNSIGNED(data);
                    WHEN r_srcu =>
                        src(22 DOWNTO 16) <= UNSIGNED(data(6 DOWNTO 0));
                    WHEN r_dstl =>
                        dst(7 DOWNTO 0) <= UNSIGNED(data);
                    WHEN r_dsth =>
                        dst(15 DOWNTO 8) <= UNSIGNED(data);
                    WHEN r_lenl =>
                        len(7 DOWNTO 0) <= UNSIGNED(data);
                    WHEN r_lenh =>
                        len(15 DOWNTO 8) <= UNSIGNED(data);
                    WHEN r_ctrl =>
                        ctrl <= STD_ULOGIC_VECTOR(data(2 DOWNTO 1));
                        fadr <= STD_ULOGIC(data(6));
                        -- Check if DMA START requested
                        IF data(0) = '1' AND data(7) = '0' THEN
                            -- Start DMA in ROM to RAM mode
                            dma <= dma_req;
                        ELSIF data(0) = '1' AND data(7) = '1' THEN
                            -- Start DMA in ROM to DAC mode
                            dma <= dma_await;
                        END IF;
                    WHEN r_div =>
                        divrst <= UNSIGNED(data);
                    WHEN r_dac =>
                        dac <= STD_ULOGIC_VECTOR(data);
                    WHEN r_roms =>
                        -- ROM bank, lower bits
                        rom_bank(7 DOWNTO 0) <= STD_ULOGIC_VECTOR(data);
                    WHEN r_romh =>
                        -- ROM bank, upper bit
                        rom_bank(8) <= STD_ULOGIC(data(0));
                    WHEN r_conf =>
                        -- Transparent addressing
                        transparent <= STD_ULOGIC(data(0));
                        wr_en <= STD_ULOGIC(data(1));
                    WHEN r_none =>
                -- Nothing to do
                END CASE; -- reg_sel
            END IF; -- iowr = '1' ...
            CASE dma IS
                WHEN dma_idle =>
                WHEN dma_req =>
                    -- Bus requested, wait until acknowledged by CPU
                    IF n_busak = '0' THEN dma <= dma_sync;
                    END IF;
                -- ROM to CPC internal RAM states
                WHEN dma_sync =>
                    -- Wait until READY set to synchronize with GA
                    IF ready = '1' THEN dma <= dma_rd;
                    END IF;
                WHEN dma_rd =>
                    -- Read is performed on falling edge.
                    -- Advance to next state.
                    dma <= dma_rdy;
                WHEN dma_rdy =>
                    -- Wait until host ready
                    IF ready = '1' THEN dma <= dma_wr; END IF;
                WHEN dma_wr =>
                    -- Check if transfer complete
                    IF len = 0 THEN
                        -- DMA transfer ended
                        dma <= dma_idle;
                    ELSE
                        -- DMA transfer did not end, prepare for next byte,
                        -- doing increment on addresses if requested.
                        len <= len - 1;
                        IF ctrl(1) = '1' THEN src <= src + 1; END IF;
                        IF ctrl(2) = '1' THEN dst <= dst + 1; END IF;
                        dma <= dma_rd;
                    END IF;
                WHEN dma_await =>
                    -- Synchronize with ready = '0', to ensure CPU is not
                    -- addressing ROM when performing reads. As reads in this
                    -- mode are performed in multiples of 4 cpu cycles, all
                    -- remaining reads will fall in the ready = '0' state.
                    IF ready = '0' THEN
                        dma <= dma_ard;
                    END IF;
                -- ROM to DAC states
                WHEN dma_ard =>
                    dma <= dma_awr;
                    divc(9 DOWNTO 2) <= divrst;
                    -- Subtract two cycles to compensate ard and awr cycles
                    divc(1 DOWNTO 0) <= "01";
                WHEN dma_awr =>
                    -- Set the timer and jump to timer wait
                    dma <= dma_atim;
                    dac <= scratch;
                WHEN dma_atim =>
                    -- Decrement timer until it expires
                    IF divc = 0 THEN
                        IF len = 0 THEN
                            dma <= dma_idle;
                        -- TODO: Interrupt logic
                        ELSE
                            len <= len - 1;
                            src <= src + 1;
                            dma <= dma_ard;
                        END IF;
                    ELSE
                        divc <= divc - 1;
                    END IF;
            END CASE; -- dma
        END IF; -- RISING_EDGE(clk4)
    END PROCESS;

    -- DMA controller: actions taking place on falling clock edges
    PROCESS(clk4)
    BEGIN
        IF FALLING_EDGE(clk4) THEN
            -- Read data on the falling edge of dma_rd state
            IF dma = dma_rd OR dma = dma_ard THEN
                scratch <= STD_ULOGIC_VECTOR(data);
            END IF;
        END IF;
    END PROCESS;


    -- UPDATE COMBINATIONAL OUTPUTS --
    n_busrq <= '1' WHEN dma = dma_idle OR dma = dma_await OR dma = dma_ard OR
               dma = dma_awr OR dma = dma_atim ELSE '0';
    -- Bridge buses when DMA is not reading from ROM
    n_busbr <= '1' WHEN dma = dma_rd OR dma = dma_ard ELSE '0';
    -- Bus master when doing ROM to RAM transfers
    dma_bm <= '0' WHEN dma = dma_idle OR dma = dma_req OR
              dma = dma_ard OR dma = dma_awr OR dma = dma_atim ELSE '1';
    n_wr   <= 'Z' WHEN dma = dma_idle OR dma = dma_req OR dma = dma_await ELSE
              '0' WHEN dma = dma_wr OR dma = dma_rdy ELSE '1';
    n_mreq <= 'Z' WHEN dma = dma_idle OR dma = dma_req OR dma = dma_await OR
              dma = dma_ard OR dma = dma_awr OR dma = dma_atim ELSE
              '0' WHEN dma = dma_wr OR dma = dma_rdy ELSE '1';

    addr   <= STD_LOGIC_VECTOR(src(15 DOWNTO 0)) WHEN dma = dma_rd OR dma = dma_ard  ELSE
              STD_LOGIC_VECTOR(dst(15 DOWNTO 0)) WHEN dma = dma_wr OR dma = dma_rdy ELSE
              (OTHERS => 'Z');
    -- Use bank 7 when accessing lorom
    fn_addr <= STD_ULOGIC_VECTOR(src(22 DOWNTO 14)) WHEN
	       ((dma = dma_rd OR dma = dma_ard) AND fadr = '1') ELSE
	       "000000111" WHEN lorom = '1' ELSE rom_bank;
    data   <= STD_LOGIC_VECTOR(scratch) WHEN dma = dma_wr OR dma = dma_rdy ELSE
	      (OTHERS => 'Z');

    -- Data transceiver
    t_dir <= '1' WHEN dma = dma_rdy OR dma = dma_wr ELSE NOT n_rd;
    -- Enable transceiver if writing to IO or reading from ROM or
    -- writing to flash or DMA writing to CPC internal RAM.
    tn_oe <= '0' WHEN iowr = '1' OR romrd = '1' OR flashwr = '1' OR
	     dma = dma_rdy OR dma = dma_wr ELSE '1';
    -- Flash (ROM) OE and CE
    romdis <= romrd; -- Disable internal CPC ROM for cart ROM reads

    -- Flash CE and OE enabled when CPU or DMA reading from ROM. CE also
    -- enabled when writing to flash
    fn_ce <= '0' WHEN romrd = '1' OR flashwr = '1' OR dma = dma_rd OR
             dma = dma_ard ELSE '1';
    fn_oe <= '0' WHEN romrd = '1' OR dma = dma_rd OR dma = dma_ard ELSE '1';
    fn_we <= NOT flashwr;

    -- Temporal signals
    ramdis <= '0';
    n_nmi <= '1';
    -- RGB LED is used only for debugging purposes
    led(2) <= NOT rom_bank(2);
    led(1) <= NOT rom_bank(1);
    led(0) <= NOT rom_bank(0);
    daco <= dac;
    sqw <= "0000";
END behavioral;

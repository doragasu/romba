-- ROMBA test.
-- NOTE: I should write a proper test, instanciating a ROM and a RAM
-- instead of manually recreating signals.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;

ENTITY romba_test IS
    END romba_test;

ARCHITECTURE romba_testbench OF romba_test IS 

    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT romba
        PORT (
                -- Z80 CPU BUSES --
                --NOTE: Most Addr lines might be removed from port
                addr    : INOUT  STD_LOGIC_VECTOR(15 DOWNTO 0);--CPU A15 to A0
                data    : INOUT  STD_LOGIC_VECTOR(7 DOWNTO 0);	--CPU D7 to D0
                n_iorq  : IN  STD_LOGIC;					--CPU IO request
                n_mreq  : INOUT  STD_LOGIC;					--Memory request
                n_rd    : IN  STD_LOGIC;					--CPU read
                n_wr    : INOUT  STD_LOGIC;                 --CPU write
                n_m1    : IN  STD_ULOGIC;
                n_rfsh  : IN  STD_ULOGIC;                   --Z80 Refresh
                n_busrq : OUT STD_ULOGIC;					--CPU BUS request
                n_busak : IN  STD_ULOGIC;                   --CPU BUS acknowledge
                n_nmi	: OUT STD_ULOGIC;					--Non maskarable int
                n_rset  : IN  STD_ULOGIC;					--Reset
                clk4    : IN  STD_ULOGIC;                   --Z80 4MHz clock

                --GA signals
                n_ramrd : IN  STD_ULOGIC;					--RAM read active
                n_romen : IN  STD_ULOGIC;					--GA ROM enable
                ready   : IN  STD_ULOGIC;					--Ready signal
                ramdis  : OUT STD_ULOGIC;					--Disable internal RAM
                romdis  : OUT STD_ULOGIC;                   --Internal ROM disable

                -- Transceiver control --
                t_dir   : OUT STD_ULOGIC;					--Direction
                tn_oe   : OUT STD_ULOGIC;					--Output enable
                n_busbr: OUT STD_ULOGIC;					--Bridge CPC & cart buses
                dma_bm  : OUT STD_ULOGIC;					--DMA Bus Master

                -- Flash chip interface --
                fn_addr: OUT STD_ULOGIC_VECTOR(22 DOWNTO 14);--Flash higher lines
                fn_ce  : OUT STD_ULOGIC;                     --Chip enable
                fn_oe  : OUT STD_ULOGIC;                     --Output enable
                fn_we  : OUT STD_ULOGIC;                     --Write enable

                -- Sound generation
                daco   : OUT STD_ULOGIC_VECTOR(7 DOWNTO 0);  --8 bit dac
                sqw    : OUT STD_ULOGIC_VECTOR(3 DOWNTO 0);  --Pulse outputs

                -- OTHER --
                led   : OUT STD_ULOGIC_VECTOR(2 DOWNTO 0)	 --RGB LED.
             );
    END COMPONENT;


    --Inputs
    SIGNAL addr    : STD_LOGIC_VECTOR(15 DOWNTO 0) := (OTHERS => 'Z');
    SIGNAL data    : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => 'Z');
    SIGNAL n_iorq  : STD_LOGIC := 'Z';
    SIGNAL n_mreq  : STD_LOGIC := 'Z';
    SIGNAL n_rd    : STD_LOGIC := 'Z';
    SIGNAL n_wr    : STD_LOGIC := 'Z';
    SIGNAL n_m1    : STD_ULOGIC := '1';
    SIGNAL n_rfsh  : STD_ULOGIC := '1';
    SIGNAL n_busak : STD_ULOGIC := '1';
    SIGNAL n_rset  : STD_ULOGIC := '0';
    SIGNAL clk4    : STD_ULOGIC := '0';
    SIGNAL n_ramrd : STD_ULOGIC := '1';
    SIGNAL n_romen : STD_ULOGIC := '1';
    SIGNAL ready   : STD_ULOGIC := '0';

    --Outputs
    SIGNAL n_busrq : STD_ULOGIC;
    SIGNAL n_nmi   : STD_ULOGIC;
    SIGNAL ramdis  : STD_ULOGIC;
    SIGNAL romdis  : STD_ULOGIC;
    SIGNAL t_dir   : STD_ULOGIC;
    SIGNAL tn_oe   : STD_ULOGIC;
    SIGNAL fn_addr : STD_ULOGIC_VECTOR(22 DOWNTO 14);
    SIGNAL fn_ce   : STD_ULOGIC;
    SIGNAL fn_oe   : STD_ULOGIC;
    SIGNAL fn_we   : STD_ULOGIC;
    SIGNAL n_busbr : STD_ULOGIC;
    SIGNAL dma_bm  : STD_ULOGIC;
    SIGNAL daco    : STD_ULOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL sqw     : STD_ULOGIC_VECTOR(3 DOWNTO 0);
    SIGNAL led     : STD_ULOGIC_VECTOR(2 DOWNTO 0);

    CONSTANT clk4_period : TIME := 250 ns;
    CONSTANT cyc_hold : TIME := 200 ns;
    CONSTANT cyc_idle : TIME := 50 ns;
--    CONSTANT srcl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC00";
--    CONSTANT srch : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC40";
--    CONSTANT srcu : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC80";
--    CONSTANT dstl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FD00";
--    CONSTANT dsth : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FD40";
--    CONSTANT lenl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FD80";
--    CONSTANT lenh : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FDC0";
--    CONSTANT ctrl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FE00";
--    CONSTANT ddiv : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FE40";
--    CONSTANT ddac : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FE80";
    CONSTANT srcl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC00";
    CONSTANT srch : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC01";
    CONSTANT srcu : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC02";
    CONSTANT dstl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC04";
    CONSTANT dsth : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC05";
    CONSTANT lenl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC06";
    CONSTANT lenh : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC07";
    CONSTANT ctrl : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC08";
    CONSTANT ddiv : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC09";
    CONSTANT ddac : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"FC0A";
    CONSTANT rsel : STD_LOGIC_VECTOR(15 DOWNTO 0) := x"DC00";

    CONSTANT dval : UNSIGNED(7 DOWNTO 0) := x"02";

--	PROCEDURE cpu_wr (addr : STD_LOGIC_VECTOR(3 DOWNTO 0); --					  data : STD_LOGIC_VECTOR(7 DOWNTO 0)) IS
--	BEGIN
--		caddr <= addr;
--		cdata <= data;
--	END cpu_wr;

BEGIN

    -- Instantiate the Unit Under Test (UUT)
    uut: romba PORT MAP (
                            addr => addr,
                            data => data,
                            n_iorq => n_iorq,
                            n_mreq => n_mreq,
                            n_rd => n_rd,
                            n_wr => n_wr,
                            n_m1 => n_m1,
                            n_rfsh => n_rfsh,
                            n_busak => n_busak,
                            n_rset => n_rset,
                            clk4 => clk4,
                            n_ramrd => n_ramrd,
                            n_romen => n_romen,
                            ready => ready,
                            n_busrq => n_busrq,
                            n_nmi => n_nmi,
                            ramdis => ramdis,
                            romdis => romdis,
                            t_dir => t_dir,
                            tn_oe => tn_oe,
                            n_busbr => n_busbr,
                            dma_bm => dma_bm,
                            fn_addr => fn_addr,
                            fn_ce => fn_ce,
                            fn_oe => fn_oe,
                            fn_we => fn_we,
                            daco => daco,
                            sqw => sqw,
                            led => led
                        );

    -- Clock process definitions
    clk4_process : PROCESS
    BEGIN
        clk4 <= TRANSPORT '0';
        WAIT FOR clk4_period/2;
        clk4 <= TRANSPORT '1';
        WAIT FOR clk4_period/2;
    END PROCESS;

    ready_process : PROCESS
    BEGIN
        ready <= TRANSPORT '0';
        WAIT FOR 6 * clk4_period / 2;
        ready <= TRANSPORT '1';
        WAIT FOR 2 * clk4_period / 2;
    END PROCESS;


    -- Stimulus process
    stim_proc: PROCESS
    BEGIN        
        -- hold reset state for 600 ns.
        WAIT FOR 600 ns;
        n_rset <= TRANSPORT '1';
        -- Wait for Reset to complete
        WAIT FOR 500 ns;
        -- Write to ROM select register
        n_iorq <= TRANSPORT '0';
        n_rd <= TRANSPORT '1';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT rsel;
        data <= TRANSPORT x"01";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        ASSERT fn_addr(21 DOWNTO 14) = x"01" REPORT "ROM select write failed!";
        -- Write to other IO address
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT x"E000";
        data <= TRANSPORT x"02";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        ASSERT fn_addr(21 DOWNTO 14) = x"01" REPORT "ROM select changed!";
        -- Read from ROM
        n_mreq <= TRANSPORT '0';
        n_romen <= TRANSPORT '0';	-- TODO: Check this
        n_rd <= TRANSPORT '0';
        addr <= TRANSPORT x"C000";
        WAIT FOR cyc_hold;
        ASSERT fn_oe = '0' AND fn_ce = '0' REPORT "ROM read failed!";
        -- Read from lower rom
        n_mreq <= TRANSPORT '0';
        n_romen <= TRANSPORT '0';
        n_rd <= TRANSPORT '0';
        addr <= TRANSPORT x"1000";
        WAIT FOR cyc_hold;
        ASSERT fn_oe = '0' AND fn_ce = '0' AND fn_addr = "000000111" REPORT "Lower ROM read failed!";
        n_mreq <= TRANSPORT '1';
        n_romen <= TRANSPORT '1';
        n_rd <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        -- Do NOT Read from ROM
        n_mreq <= TRANSPORT '0';
        n_rd <= TRANSPORT '0';
        addr <= TRANSPORT x"C001";
        WAIT FOR cyc_hold;
        n_mreq <= TRANSPORT '1';
        n_rd <= TRANSPORT '1';
        ASSERT fn_ce = '1' REPORT "ROM should not have readed!";
        WAIT FOR cyc_idle;
        -- Read from ROM
        n_mreq <= TRANSPORT '0';
        n_romen <= TRANSPORT '0';	-- TODO: Check this
        n_rd <= TRANSPORT '0';
        addr <= TRANSPORT x"C002";
        WAIT FOR cyc_hold;
        n_mreq <= TRANSPORT '1';
        n_romen <= TRANSPORT '1';
        n_rd <= TRANSPORT '1';
        ASSERT fn_oe = '0' AND fn_ce = '0' REPORT "ROM read failed!";
        WAIT FOR cyc_idle;
        -- Write to DAC
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT ddac;
        data <= TRANSPORT x"7E";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        ASSERT daco = x"7E" REPORT "Write to DAC failed";
        -- Write to Flash
        n_wr <= TRANSPORT '0';
        n_romen <= TRANSPORT '0';
        addr <= TRANSPORT x"CAAA";
        data <= TRANSPORT x"AA";
        WAIT FOR cyc_hold;
        ASSERT fn_we = '0' AND fn_ce = '0' REPORT "Flash write failed!";
        n_wr <= TRANSPORT '1';
        n_romen <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write to ROM select register
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT rsel;
        data <= TRANSPORT x"09";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        ASSERT fn_addr(21 DOWNTO 14) = x"09" REPORT "ROM select write failed!";
        -- Write source address to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srcl;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srch;
        data <= TRANSPORT x"C0";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write destination address to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT dstl;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT dsth;
        data <= TRANSPORT x"10";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write destination transfer length to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenl;
        data <= TRANSPORT x"02";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenh;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Start DMA with auto-increments
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT ctrl;
        data <= TRANSPORT x"07";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Wait for n_busrq signal
        WAIT UNTIL clk4'EVENT AND clk4 = '1' AND n_busrq = '0';
        -- Wait a cycle and assert bus
        WAIT FOR clk4_period + cyc_idle;
        n_busak <= '0';
        addr <= (OTHERS => 'Z');
        data <= (OTHERS => 'Z');
        n_wr <= 'Z';
        n_rd <= 'Z';
        n_iorq <= 'Z';
        n_mreq <= 'Z';
        -- Waiting for READY signal to sync DMA
--	WAIT until ready'EVENT and ready = '1';
        WAIT until ready'EVENT and ready = '1';
        WAIT until clk4'event and clk4 = '1';
        -- Flash puts the first byte
        data <= x"DE";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        WAIT UNTIL ready'EVENT and ready = '0';
        -- Flash puts the second byte
        WAIT UNTIL clk4'EVENT AND clk4 = '1';
        data <= x"AD";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        WAIT UNTIL ready'EVENT and ready = '0';
        -- Flash puts the third byte
        WAIT UNTIL clk4'EVENT AND clk4 = '1';
        data <= x"7F";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        -- Wait until DMA releases the bus
        WAIT UNTIL clk4'EVENT AND clk4 = '1' AND n_busrq = '1';
        n_rd <= '1';
        n_wr <= '1';
        n_iorq <= '1';
        n_mreq <= '1';
        n_busak <= '1';
        addr <= (OTHERS => 'Z');
        WAIT FOR cyc_hold;

        -- Write source address to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srcl;
        data <= TRANSPORT x"55";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srch;
        data <= TRANSPORT x"D0";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srcu;
        data <= TRANSPORT x"55";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write destination address to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT dstl;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT dsth;
        data <= TRANSPORT x"10";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write destination transfer length to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenl;
        data <= TRANSPORT x"02";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenh;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Start DMA with auto-increments and full addressing
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT ctrl;
        data <= TRANSPORT x"47";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        addr <= (OTHERS => 'Z');
        -- Wait for n_busrq signal
        WAIT UNTIL clk4'EVENT AND clk4 = '1' AND n_busrq = '0';
        -- Wait a cycle and assert bus
        WAIT FOR clk4_period + cyc_idle;
        n_busak <= '0';
        addr <= (OTHERS => 'Z');
        data <= (OTHERS => 'Z');
        n_wr <= 'Z';
        n_rd <= 'Z';
        n_iorq <= 'Z';
        n_mreq <= 'Z';
        -- Waiting for READY signal to sync DMA
--	WAIT until ready'EVENT and ready = '1';
        WAIT until ready'EVENT and ready = '1';
        WAIT until clk4'event and clk4 = '1';
        -- Flash puts the first byte
        data <= x"DE";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        WAIT UNTIL ready'EVENT and ready = '0';
        -- Flash puts the second byte
        WAIT UNTIL clk4'EVENT AND clk4 = '1';
        data <= x"AD";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        WAIT UNTIL ready'EVENT and ready = '0';
        -- Flash puts the third byte
        WAIT UNTIL clk4'EVENT AND clk4 = '1';
        data <= x"7F";
        WAIT FOR clk4_period;
        data <= (OTHERS => 'Z');
        -- Wait until DMA releases the bus
        WAIT UNTIL clk4'EVENT AND clk4 = '1' AND n_busrq = '1';
        n_busak <= '1';
        n_rd <= '1';
        n_wr <= '1';
        n_iorq <= '1';
        n_mreq <= '1';
        addr <= (OTHERS => '0');
        -- Prepare ROM to DAC DMA test --
        WAIT FOR cyc_hold;
        -- Write source address to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srcl;
        data <= TRANSPORT x"10";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT srch;
        data <= TRANSPORT x"A0";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write transfer length to DMA
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenl;
        data <= TRANSPORT x"03";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT lenh;
        data <= TRANSPORT x"00";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Write divisor value
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT ddiv;
        data <= TRANSPORT STD_LOGIC_VECTOR(dval);
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        data <= (OTHERS => 'Z');
        -- Start ROM to DAC DMA (NOTE: src auto increments are required).
        n_iorq <= TRANSPORT '0';
        n_wr <= TRANSPORT '0';
        addr <= TRANSPORT ctrl;
        data <= TRANSPORT x"C1";
        WAIT FOR cyc_hold;
        n_iorq <= TRANSPORT '1';
        n_wr <= TRANSPORT '1';
        WAIT FOR cyc_idle;
        addr <= TRANSPORT (OTHERS => 'Z');
        data <= TRANSPORT (OTHERS => 'Z');
        WAIT FOR cyc_idle;
        -- First DAC byte
        data <= TRANSPORT x"80";
        WAIT FOR clk4_period;
        data <= TRANSPORT (OTHERS => 'Z');
--	WAIT FOR ((4 - 1) * INTEGER(ddiv)) * clk4_period;
        WAIT FOR ((2 + 1) * 4 - 1) * clk4_period;
        data <= TRANSPORT x"A0";
        WAIT FOR clk4_period;
        data <= TRANSPORT (OTHERS => 'Z');
        WAIT FOR ((2 + 1) * 4 - 1) * clk4_period;
        data <= TRANSPORT x"40";
        WAIT FOR clk4_period;
        data <= TRANSPORT (OTHERS => 'Z');
        WAIT FOR ((2 + 1) * 4 - 1) * clk4_period;
        data <= TRANSPORT x"20";
        WAIT FOR clk4_period;
        data <= TRANSPORT (OTHERS => 'Z');


        WAIT;
    END PROCESS;

END;

CONFIGURATION romba_cfg OF romba_test IS
    FOR romba_testbench
        END FOR;
    END romba_cfg;


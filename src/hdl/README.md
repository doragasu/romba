# Introduction

Here you can find the HDL resources for the CPLD in ROMBA cartridges. Inside `romba` directory you will find the `romba.vhd` source that must be synthesized and burned into the CPLD, along with the files required to perform the simulation. Inside `lromba` directory you will find the project files you can load into Lattice Diamond suite to build the bitfile and burn it into the CPLD.

# Simulating the design

Simulation is run using [GHDL](http://ghdl.free.fr/), and waveforms are displayed using GTKWave. So you will need to install both packages in your system to be able to run it. Once done, to run the simulation, I have created a simple makefile, so just open your favorite terminal, change to the `src/hdl/romba` directory and run the `make` command. Almost instantly, if any assertion fails in the simulation it will be printed in the console, and also a GTKWave window will pop up showing the waveforms. You can add and remove waveforms as needed, and when finished my recommendation is for you to update the Save file `romba_test.gtkw`, for the layout to be loaded on next session. Assertions in the `romba_test.vhd` test bench are far from being exhaustive, so you must have a look to the waveforms to make sure everything is OK.

If you are brave enough, you can try making modifications to the HDL, but be careful because currently the design uses 96% of the LUTs, so unless you remove some functionality to implement other, or switch to a bigger (and pricier) CPLD, there is not much room for improvement. If you make notifications, please drop me a line, I would like to see them!

# Creating and burning the bitfile

## Creating the bitfile

To synthesize the design and create the bitfile, you need [Lattice Diamond](https://www.latticesemi.com/latticediamond). You can request a free (as in beer) license, and it will be enough for this task. This file does not pretend to be a step by step guide of the tool. But basically what you have to do is opening the `lromba.ldf` project file, then go to the `Process` tab on the left pane, and then click `Process / Run`. The build should complete without problem.

## Programming the bitfile

Once the bitfile is generated, carefully plug the cartridge to your favorite programmer. Make sure pinout of the edge connector is OK, and that the board is properly powered. Then open programmer application (`Tools / Programmer`), verify the device is dected correctly (it should display a LAMXO256C or LCMXO256C) and burn the file. The process should complete without errors.

## About the programmer

You can find ready-to-use programmers for Lattice chips. But if you want to save some bucks and have a FT232 based board floating around (or can buy one), you can use it to program the CPLD. It works really well and program process is very fast. I might extend this section a bit if I get the time and this picks some interest.

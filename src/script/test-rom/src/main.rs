use std::fs::File;
use std::io::{self, Read, Write};

const ROM_LEN: usize = 16384;
const INPUT_ROM: &str = "slot7.bin";
const OUTPUT_ROM: &str = "output.bin";

fn fill_bank(bank: u16, rom: &mut [u8; ROM_LEN]) {
    let mut i = 0;
    while i < (ROM_LEN / 2) as u16 {
        let val = i + bank;
        rom[(2 * i) as usize] = (val / 256) as u8;
        rom[(2 * i + 1) as usize] = (val & 0xFF) as u8;
        i = i + 1;
    }
}

fn read_file_into_array(file_name: &str, buffer: &mut [u8; 16384]) -> io::Result<usize> {
    let mut file = File::open(file_name)?;
    file.read(buffer)
}

fn main() {
    let mut rom: [u8; ROM_LEN] = [0; ROM_LEN];
    let mut bank: u16 = 0;

    // Open output file
    let mut out = File::create(OUTPUT_ROM).unwrap();

    // Iterate through all banks to create ROM
    while bank < 512 {
        match bank {
            7 => _ = read_file_into_array(INPUT_ROM, &mut rom)
                .expect("missing slot 7 ROM \"slot7.bin\""),
            _other => fill_bank(bank, &mut rom)
        }
        // Write bank
        out.write(&rom).unwrap();
        bank = bank + 1;
    }
}

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
ROMARY: ROM ArRaY

This quick and dirty script allows building collections of ROMs. I has been
created for using with ROMBA cartridges, but it might be useful for other
devices too.

You will need PyQt5 to run it. See the README.md file for more details.

License: GPLv3
"""

import sys, os
from PyQt5.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout, \
    QDialog, QFileDialog, QCheckBox, QApplication, QComboBox, QListWidget,  \
    QLabel, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont


class RomarioDiag(QDialog):
    def __init__(self):
        super().__init__()
        self.initUI()
        # Load previous list file if available
        if os.path.isfile('output.rlf'):
            self.parseListFile('output.rlf')

    def initUI(self):
        """ Initialize dialog """

        # Create file list widgets
        flabel = QLabel('ROM list:')
        self.fname = QListWidget()
        for i in range(0, 32):
            self.fname.addItem(str(i) + ': ')

        # Add default ROMs for BASIC and OS
        self.fname.item(0).setText('0: BASIC1.1_(6128_Spanish).ROM')
        self.fname.item(7).setText('7: FW312ES.ROM')
        self.fname.setCurrentRow(1)
        fdiagBtn = QPushButton('🖉')
        fdiagBtn.setFixedWidth(30)
        fdiagBtn.setFixedHeight(30)
        fdiagBtn.clicked.connect(self.selectFile)
        fclrBtn = QPushButton('🗑')
        font = fclrBtn.font()
        font.setPointSize(14)
        fclrBtn.setFont(font)
        fclrBtn.setFixedWidth(30)
        fclrBtn.setFixedHeight(30)
        fclrBtn.clicked.connect(self.clrCurrent)
        floadBtn = QPushButton('🗁')
        floadBtn.setFixedWidth(30)
        floadBtn.setFixedHeight(30)
        floadBtn.setFont(font)
        floadBtn.clicked.connect(self.loadListFile)
        # Create check box
        self.autoCb = QCheckBox('Trim headers')
        self.autoCb.setCheckState(Qt.Checked)
        # Create combo box (with label)
        clabel = QLabel('Generate: ')
        self.combo = QComboBox()
        self.combo.addItems(['All', 'BIN', 'List'])
        # Create OK/Exit buttons
        okBtn = QPushButton('GO!')
        okBtn.clicked.connect(self.go)
        okBtn.setDefault(True)      # This is the default (accept) button
        cancelBtn = QPushButton('Exit')
        cancelBtn.clicked.connect(self.reject)

        # Arrange widget layouts
        vbFButtons = QVBoxLayout()
        vbFButtons.setAlignment(Qt.AlignTop)
        vbFButtons.addWidget(fdiagBtn)
        vbFButtons.addWidget(fclrBtn)
        vbFButtons.addWidget(floadBtn)

        hbFlist = QHBoxLayout()
        hbFlist.addWidget(self.fname)
        hbFlist.addLayout(vbFButtons)

        hbCombo = QHBoxLayout()
        hbCombo.setAlignment(Qt.AlignLeft)
        hbCombo.addWidget(clabel)
        hbCombo.addWidget(self.combo)
        hbCombo.addWidget(self.autoCb)

        hbOkCan = QHBoxLayout()
        hbOkCan.setAlignment(Qt.AlignRight)
        hbOkCan.addWidget(okBtn)
        hbOkCan.addWidget(cancelBtn)

        vbMain = QVBoxLayout()
        vbMain.addWidget(flabel)
        vbMain.addLayout(hbFlist)
        vbMain.addLayout(hbCombo)
        vbMain.addSpacing(20)
        vbMain.addLayout(hbOkCan)

        self.setLayout(vbMain)
        self.setWindowTitle('ROMARY')

    def msgBox(self, icon, title, text):
        msg = QMessageBox()
        msg.setIcon(icon)
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.exec_()

    def clrFile(self, pos):
        item = self.fname.item(pos)
        item.setText(str(pos) + ': ')

    def clrCurrent(self):
        self.clrFile(self.fname.currentRow())

    def fileDialog(self, title, extension):
        dlg = QFileDialog(self)
        dlg.setWindowTitle(title)
        dlg.setViewMode(QFileDialog.Detail)
        dlg.setNameFilters([self.tr(extension), self.tr('All Files (*)')])
        dlg.setFileMode(QFileDialog.ExistingFile)

        fname = None
        if dlg.exec_():
            fname = dlg.selectedFiles()

        if fname != None:
            fname = fname[0]

        return fname

    def setFile(self, row, file_name):
        self.fname.item(row).setText(str(row) + ': ' + file_name)

    def selectFile(self):
        fname = self.fileDialog('Select ROM', 'Amstrad CPC ROM files (*.rom *.ROM)')

        if fname != None:
            row = self.fname.currentRow()
            self.setFile(row, fname)
            if row < 31:
                self.fname.setCurrentRow(row + 1)

    def parseListFile(self, file_name):
        fdin = open(file_name, "r")
        i = 0
        line = fdin.readline()
        while line and i < 32:
            line = line.strip()
            self.fname.item(i).setText(line)
            line = fdin.readline()
            i = i + 1

        self.fname.setCurrentRow(0)
        fdin.close()

    def loadListFile(self):
        fname = self.fileDialog('Select ROM list file', 'ROM list files (*.rlf *.RLF)')

        if fname != None:
            for i in range(0, 32):
                self.clrFile(i)

            self.parseListFile(fname)


    def getFilename(self, item_num):
        """ Extract filename and file index """
        itemStr = self.fname.item(item_num).text()
        pos = itemStr.find(":");
        return itemStr[pos + 2:]

    def addFile(self, fdout, file_name):
        fdin = open(file_name, "rb")
        err = 0
        in_len = os.path.getsize(file_name)

        if in_len == 16512:
            # Skip 128 byte header
            fdin.seek(128)
            in_len = in_len - 128

        if in_len == 16384:
            fdout.write(fdin.read(16384))
        else:
            err = 1

        fdin.close()
        return err 

    def joinFiles(self, last_rom):
        fdout = open('output.bin', 'wb')
        empty = b'\xff' * 16384

        for i in range(0, last_rom + 1):
            file_name = self.getFilename(i)
            if len(file_name) <= 0:
                # No file, add empty buffer
                fdout.write(empty)
            else:
                if self.addFile(fdout, file_name) != 0:
                    self.msgBox(QMessageBox.Warning, 'Parsing error',
                            'Failed to parse file: ' + file_name)
                    return -1

        fdout.close()
        return 0

    def writeList(self, last_rom):
        fdout = open('output.rlf', 'w')
        for i in range(0, last_rom + 1):
            fdout.write(self.fname.item(i).text())
            fdout.write('\n')

        fdout.close()
        return 0

    def go(self):
        """ ROM assembly goes here """
        # Determine the length of the ROM
        last_rom = 31
        while last_rom >= 0:
            filename = self.getFilename(last_rom)
            if len(filename) > 0:
                break
            last_rom = last_rom - 1

        if last_rom < 0:
            self.msgBox(QMessageBox.Information, 'No ROMs added',
                    'Add some ROMs and try again!')
            return

        if self.combo.currentText() == "All" or self.combo.currentText == "BIN":
            if self.joinFiles(last_rom) != 0:
                self.msgBox(QMessageBox.Warning, 'Error',
                        'Failed to create output file')
                return

        if self.combo.currentText() == "All" or self.combo.currentText == "List":
            if self.writeList(last_rom) != 0:
                self.msgBox(QMessageBox.Warning, 'Error',
                        'Failed to create list file')
                return

        self.msgBox(QMessageBox.Information, 'Done',
                'Process completed successfully!')

if __name__ == '__main__':
    """ Main dialog """
    
    app = QApplication(sys.argv)
    win = RomarioDiag()
    # Set width, set height to the minimum
    win.resize(500, 500)
    sys.exit(win.exec_())


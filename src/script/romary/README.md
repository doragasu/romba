# Introduction

ROMARY (ROM ArRaY) is a quick and dirty script that allows building collections of ROMs. It has been created for using with ROMBA cartridges, but it might be useful for other devices too.

# Running

To run the `romary.py` script, you need `Python 3` and the `PyQt5` package. On Linux you should be able to install these dependencies easily using your distro of choice package manager, and most likely you will have them already.

To start `romary.py` just run this script. You can do it directly from the terminal or create a symlink on your favorite desktop environment.

# Usage

ROMARY GUI shows a list with 32 ROM slots. By default it loads in these slots the ROMs corresponding to the last compilation you did. On the first run, it loads on slots 0 and 7 the BASIC and Firmware ROMs corresponding to a spanish CPC6128 with support for 32 ROMs (note that default firmware on a CPC464 initializes 7 ROMs, and on a CPC6128 it initializes 16 ROMs). Note that if these ROMs are not present, collection generation will fail, so make sure you have them or replace the ROMs in these slots with the ones you want.

![ROMARY GUI](romary.png)

To the right of the ROM list there are 3 buttons:

* `🖉`: Load a ROM in the selected slot.
* `🗑`: Clear the ROM in the selected slot.
* `🗁`: Load a `.rlf` file, with a ROM list.

Use the `🖉` button to load all the ROMs you want, or load a previously defined list with the `🗁` button. If you want to clear a ROM entry, select it and use the `🗑` button. Then set the `Generate` combo box below the ROM list to the desired value:

* All: generate the `output.bin` ROM collection and the `output.rlf` ROM list file.
* BIN: only generate the `output.bin` ROM collection.
* List: Only generate the `output.rlf` ROM list file.

You should usually leave checked the `Trim headers` option. This option discards the 128 bytes extra header found in some ROMs, that must not be burned to the device.

When finished, click the `GO!` button. This will generate the output files according to the selection in the `Generate` combo box.

## Tips for using with ROMBA

If you are using this script to generate a compilation for a ROMBA cartridge, and you are using normal ROMs that are started via RSX commands, you need to put a firmware ROM on slot 7 and a Basic ROM on slot 0. Otherwise the machine will not boot. For the Basic you can use a copy of the Basic ROM of your machine. For the firmware it is recommended to use a firmware ROM patched to initialize the 32 ROM slots.

Also note that in my experience putting too many ROMs on the same compilation usually causes some of them to not work properly (crash or not even start when invoked). I have not investigated this problem, but I suspect that putting too many RSX extensions in RAM might cause the memory to overflow.

# License

This program is provided with NO WARRANTY of any type under the GPLv3 license. Contributions are welcome.

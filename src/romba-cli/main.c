
//=============================================================================
// LIBS
//=============================================================================

#include "util.h"
#ifdef __OS_WIN
#include <windows.h>
#else
#include <sys/ioctl.h>
#include <signal.h>
#endif

#include <getopt.h>
#include <errno.h>
#include "commands.h"
#include "progbar.h"
#include "mdma.h"

// Maximun number of characters of an address range
#define MAX_IO_RANGE_CHARS	11

/// Commandline flags (for arguments without parameters).
typedef struct {
	union {
		struct {
			uint8_t verify:1;	/// Verify flash write if TRUE
			uint8_t verbose:1;	/// Print extra information on screen
			uint8_t flashId:1;	/// Show flash chip id
			uint8_t erase:1;	/// Erase flash
			uint8_t auto_erase:1;	/// Automatically erase flash range
			uint8_t pushbutton:1;	/// Read pushbutton status
			uint8_t dry:1;		/// Dry run
			uint8_t boot:1;		/// Enter bootloader
		};
		uint8_t all;
	};
	int cols;
} Flags;

/// Prints text only if (verbose==TRUE)
#define PrintVerb(...)	do{if(f.verbose)printf(__VA_ARGS__);}while(0)

//=============================================================================
// VARS
//=============================================================================
extern libusb_device_handle *mdma_handle;
extern libusb_device *mdma_dev;


static const struct option opt[] = {
	{"flash",       required_argument,  NULL,   'f'},
	{"read",        required_argument,  NULL,   'r'},
	{"erase",       no_argument,        NULL,   'e'},
	{"sect-erase",  required_argument,  NULL,   's'},
	{"range-erase", required_argument,  NULL,   'A'},
	{"auto-erase",  no_argument,        NULL,   'a'},
	{"verify",      no_argument,        NULL,   'V'},
	{"flash-id",    no_argument,        NULL,   'i'},
	{"pushbutton",  no_argument,        NULL,   'p'},
	{"gpio-ctrl",   required_argument,  NULL,   'g'},
	{"bootloader",  no_argument,        NULL,   'b'},
	{"write-io",    required_argument,  NULL,   'w'},
	{"dry-run",     no_argument,        NULL,   'd'},
	{"version",     no_argument,        NULL,   'R'},
	{"verbose",     no_argument,        NULL,   'v'},
	{"help",        no_argument,        NULL,   'h'},
	{NULL,          0,                  NULL,    0 }
};

const char * const description[] = {
	"Flash rom file",
	"Read ROM/Flash to file",
	"Erase Flash",
	"Erase flash sector",
	"Erase flash memory range",
	"Auto-erase (use it with flash command)",
	"Verify flash after writing file",
	"Obtain flash chip identifiers",
	"Pushbutton status read (bit 1:event, bit0:pressed)",
	"Manual GPIO control (dangerous!)",
	"Switch to bootloader mode",
	"Write byte to I/O address",
	"Dry run: don't actually do anything",
	"Show program version",
	"Show additional information",
	"Print help screen and exit"
};

//=============================================================================
// FUNCTION DECLARATIONS
//=============================================================================

static void romba_exit(void)
{
#ifndef __OS_WIN
	// Restore cursor
	printf("\e[?25h");
#endif
	if (mdma_handle) {
		libusb_release_interface(mdma_handle, 0);
		libusb_close(mdma_handle);
	}

	libusb_exit(NULL);
}

#ifndef __OS_WIN
static void handler(int signum)
{
	(void)signum;

	romba_exit();
	exit(1);
}

static void install_sig_handler(void)
{
	struct sigaction sa;

	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, NULL) == -1) {
		puts("WARNING: could not install signal handler");
	}
}
#endif //!__OS_WIN

static void PrintVersion(char *prgName) {
	printf("%s version %d.%d, 2017 ~ 2018. Based on mdma by Manveru & doragasu.\n", prgName,
			VERSION_MAJOR, VERSION_MINOR);
}

static void PrintHelp(char *prgName) {
	int i;

	PrintVersion(prgName);
	printf("Usage: %s [OPTIONS [OPTION_ARG]]\nSupported options:\n\n", prgName);
	for (i = 0; opt[i].name; i++) {
		printf(" -%c, --%s%s: %s.\n", opt[i].val, opt[i].name,
				opt[i].has_arg == required_argument?" <arg>":"",
				description[i]);
	}
	// Print additional info
	printf("For file arguments, it is possible to specify start address and "
			"file length to read/write in bytes, with the following format:\n"
			"    file_name:memory_address:file_length\n\n"
			"Examples:\n"
			" - Erase Flash and write entire ROM to cartridge: %s -ef rom_file\n"
			" - Flash and verify 32 KiB to 0x700000: "
			"%s -Vf rom_file:0x700000:32768\n"
			" - Dump 1 MiB of the cartridge: %s -r rom_file::1048576\n",
			prgName, prgName, prgName);

}

//-----------------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------------
int main( int argc, char **argv )
{
	/// Command-line flags
	Flags f;
	/// Sector erase address. Set to UINT32_MAX for none
	uint32_t sect_erase = UINT32_MAX;
	/// Manual GPIO control
	// TODO: Replace with suitable structure
	int gpioCtl = FALSE;
	/// Rom file to write to flash
	MemImage fWr = {NULL, 0, 0};
	/// Rom file to read from flash (default read length: 4 MiB)
	MemImage fRd = {NULL, 0, 4*1024*1024};
	/// Error code for function calls
	int errCode;
	/// Buffer for writing data to cart
	u8 *write_buffer = NULL;
	/// Buffer for reading cart data
	u8 *read_buffer = NULL;
	/// Address for memory erase operations
	uint32_t eraseAddr = 0;
	/// Length for memory erase operations
	uint32_t eraseLen = 0;
	/// Address for IO operations
	uint32_t io_addr = 0;
	/// Data for IO operations
	uint32_t io_data = 0;
	// Just for loop iteration
	int i;

#ifndef __OS_WIN
	install_sig_handler();
#endif
	// Set all flags to FALSE
	f.all = 0;
	// Reads console arguments
	if( argc > 1 )
	{
		/// Option index, for command line options parsing
		int opIdx = 0;
		/// Character returned by getopt_long()
		int c;

		while ((c = getopt_long(argc, argv, "f:r:es:A:aVipg:bw:dRvh", opt, &opIdx)) != -1)
		{
			// Parse command-line options
			switch (c)
			{
				case 'f': // Write flash
					fWr.file = optarg;
					if ((errCode = ParseMemArgument(&fWr))) {
						PrintErr("Error: On Flash file argument: ");
						PrintMemError(errCode);
						return 1;
					}
					break;

				case 'r': // Read flash
					fRd.file = optarg;
					if ((errCode = ParseMemArgument(&fRd))) {
						PrintErr("Error: On ROM/Flash read argument: ");
						PrintMemError(errCode);
						return 1;
					}
					break;

				case 'e': // Erase entire flash
					{
						f.erase = TRUE;
					}
					break;

				case 's': // Erase sector
					sect_erase = strtol( optarg, NULL, 16 );
					break;

				case 'A': // Erase range
					if ((errCode = ParseMemRange(optarg, &eraseAddr, &eraseLen)) ||
							(0 == eraseLen)) {
						PrintErr("Error: Invalid Flash erase range argument: %s\n", optarg);
						return 1;
					}
					break;

				case 'a': // Auto erase
					f.auto_erase = TRUE;
					break;

				case 'V': // Verify flash write
					f.verify = TRUE;
					break;

				case 'i': // Flash id
					f.flashId = TRUE;
					break;

				case 'p': // Read pushbutton
					f.pushbutton = TRUE;
					break;

				case 'g': // GPIO control
					gpioCtl = TRUE;
					break;

				case 'b': // Bootloader mode
					f.boot = TRUE;
					break;

				case 'w': // Write to IO port
					if (ParseMemRange(optarg, &io_addr, &io_data)) {
						PrintErr("invalid IO write pair: %s\n", optarg);
					}
					break;

				case 'd': // Dry run
					f.dry = TRUE;
					break;

				case 'R': // Version
					PrintVersion(argv[0]);
					return 0;

				case 'v': // Verbose
					f.verbose = TRUE;
					break;

				case 'h': // Help
					PrintHelp(argv[0]);
					return 0;

				case '?':       // Unknown switch
					putchar('\n');
					PrintHelp(argv[0]);
					return 1;
			}
		}
	}
	else
	{
		printf("Nothing to do!\n");
		PrintHelp(argv[0]);
		return 0;
	}

	if (optind < argc) {
		PrintErr("Unsupported parameter:");
		for (i = optind; i < argc; i++) PrintErr(" %s", argv[i]);
		PrintErr("\n\n");
		PrintHelp(argv[0]);
		return -1;
	}

	// Sanity checks
	if (f.auto_erase && !fWr.file) {
		PrintErr("Cannot auto-erase without writing to flash!\n");
		return -1;
	}
	if (f.auto_erase && (sect_erase != UINT32_MAX)) {
		PrintErr("Auto-erase and sector erase requested, aborting!\n");
		return -1;
	}
	if (f.auto_erase && f.erase) {
		PrintErr("Auto-erase and full erase requested, aborting!\n");
		return -1;
	}
	if (f.auto_erase && eraseLen) {
		PrintErr("Auto-erase and range erase requested, aborting!\n");
		return -1;
	}
	if ((sect_erase != UINT32_MAX) && eraseLen) {
		PrintErr("Sector erase and range erase requested, aborting!\n");
		return -1;
	}
	if ((sect_erase != UINT32_MAX) && f.erase) {
		PrintErr("Sector erase and full erase requested, aborting!\n");
		return -1;
	}
	if (eraseLen && f.erase) {
		PrintErr("Full erase and range erase requested, aborting!\n");
		return -1;
	}

	if (f.verbose) {
		printf("\nThe following actions will%s be performed (in order):\n",
				f.dry?" NOT":"");
		printf("==================================================%s\n\n",
				f.dry?"====":"");
		if (io_addr) printf(" - Write %d to IO address 0x%X\n", io_data,
				io_addr);
		if (f.flashId) printf(" - Show Flash chip identification.\n");
		if (f.erase) printf(" - Erase Flash.\n");
		else if(f.auto_erase) printf(" - Auto-erase flash.\n");
		else if (eraseLen) {
			printf(" - Erase range 0x%X:%X.\n", eraseAddr, eraseLen);
		} else if (sect_erase != UINT32_MAX) 
			printf(" - Erase sector at 0x%X.\n", sect_erase);
		if (fWr.file) {
			printf(" - Flash %s", f.verify?"and verify ":"");
			PrintMemImage(&fWr); putchar('\n');
		}
		if (fRd.file) {
			printf(" - Read ROM/Flash to ");
			PrintMemImage(&fRd); putchar('\n');
		}
		if (f.pushbutton) {
			printf(" - Read pushbutton.\n");
		}
		if (gpioCtl) {
			printf(" - GPIO control (TODO).\n");
		}
		if (f.boot) {
			printf(" - Enter bootloader\n");
		}
		printf("\n");
	}

	if (f.dry) return 0;

	// Detect number of columns (for progress bar drawing).
#ifdef __OS_WIN
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	f.cols = csbi.srWindow.Right - csbi.srWindow.Left;
#else
	struct winsize max;
	ioctl(0, TIOCGWINSZ , &max);
	f.cols = max.ws_col;

	// Also set transparent cursor
	printf("\e[?25l");
#endif
	// Init libusb
	int r = libusb_init(NULL);
	if (r < 0) {
		PrintErr( "Error: could not init libusb\n" );
		PrintErr( "   Code: %s\n", libusb_error_name(r) );
		return -1;
	}

	// Uncomment this to flood the screen with libusb debug information
	//libusb_set_debug(NULL, LIBUSB_LOG_LEVEL_DEBUG);


	// Detecting romba device
	mdma_handle = libusb_open_device_with_vid_pid( NULL, MeGaWiFi_VID, MeGaWiFi_PID );

	if( mdma_handle == NULL ) {
		PrintErr( "Error: could not open device %.4X : %.4X\n", MeGaWiFi_VID, MeGaWiFi_PID );
		return -1;
	}
	else
		PrintVerb( "Completed: opened device %.4X : %.4X\n", MeGaWiFi_VID, MeGaWiFi_PID );

	mdma_dev = libusb_get_device( mdma_handle );


	// Set romba configuration
	r = libusb_set_configuration( mdma_handle, MeGaWiFi_CONFIG );
	if( r < 0 ) {
		PrintErr( "Error: could not set configuration #%d\n", MeGaWiFi_CONFIG );
		PrintErr( "   Code: %s\n", libusb_error_name(r) );
		return -1;
	}
	else
		PrintVerb( "Completed: set configuration #%d\n", MeGaWiFi_CONFIG );


	// Claiming romba interface
	r = libusb_claim_interface( mdma_handle, MeGaWiFi_INTERF );
	if( r != LIBUSB_SUCCESS )
	{
		PrintErr( "Error: could not claim interface #%d\n", MeGaWiFi_INTERF );
		PrintErr( "   Code: %s\n", libusb_error_name(r) );
		return -1;
	}
	else
		PrintVerb( "Completed: claim interface #%d\n", MeGaWiFi_INTERF );


	/****************** ↓↓↓↓↓↓ DO THE MAGIC HERE ↓↓↓↓↓↓ *******************/

	// Default exit status: OK
	errCode = 0;

	if (io_addr) {
		MDMA_io_write(io_addr, io_data);
	}

	// GET IDs	
	if (f.flashId) {
		MDMA_manId_get();
		MDMA_devId_get();
	}
	// Erase
	if (f.erase) {
		printf("Erasing cart... ");
		fflush(stdout);
		// It looks like text doesn't appear until MDMA_cart_erase()
		// completes, so flush output to force it.
		if (MDMA_cart_erase() < 0) {
			printf("ERROR!\n");
			return 1;
		}
		else printf("OK!\n");
	} else if (sect_erase != UINT32_MAX) {
		printf("Erasing sector 0x%06X...\n", sect_erase);
		MDMA_sect_erase(sect_erase);
	} else if (eraseLen) {
		printf("Erasing range 0x%X:%X...\n", eraseAddr, eraseLen);
		MDMA_range_erase(eraseAddr, eraseLen);
	}

	// Flash
	if (fWr.file) {
		write_buffer = AllocAndFlash(&fWr, f.auto_erase, f.cols);
		if (!write_buffer) {
			errCode = 1;
			goto dealloc_exit;
		}
	}

	if (fRd.file || f.verify) {
		// If verify is set, ignore addr and length set in command line.
		if (f.verify) {
			fRd.addr = fWr.addr;
			fRd.len  = fWr.len;
		}
		read_buffer = AllocAndRead(&fRd, f.cols);
		if (!read_buffer) {
			errCode = 1;
			goto dealloc_exit;
		}
		// Verify
		if (f.verify) {
			for (i = 0; i < (int)fWr.len; i++) {
				if (write_buffer[i] != read_buffer[i]) {
					break;
				}
			}
			if (i == (int)fWr.len)
				printf("Verify OK!\n");
			else {
				printf("Verify failed at addr 0x%07X!\n", i + fWr.addr);
				printf("Wrote: 0x%04X; Read: 0x%04X\n", write_buffer[i],
						read_buffer[i]);
				// Set error, but we do not exit yet, because user might want
				// to write readed data to a file!
				errCode = 1;
			}
		}
		// Write file
		if (fRd.file) {
			FILE *dump = fopen(fRd.file, "wb");
			if (!dump) {
				perror(fRd.file);
				errCode = 1;
				goto dealloc_exit;
			}
			fwrite(read_buffer, fRd.len, 1, dump);
			fclose(dump);
			printf("Wrote file %s.\n", fRd.file);
		}
	}

	if (f.pushbutton) {
		u16 retVal;
		u8 butStat;
		retVal = MDMA_button_get(&butStat);
		if (retVal < 0) {
			errCode = 1;
		} else {
			PrintVerb("Button status: 0x%02X.\n", butStat);
			errCode = butStat;
		}
		goto dealloc_exit;
	}

	if (gpioCtl)
		printf("Manual GPIO control not supported. Ignoring argument!!!\n");

dealloc_exit:
	if (write_buffer) free(write_buffer);
	if (read_buffer)  free(read_buffer);

	// Bootloader command is not replied!
	if (f.boot) MDMA_bootloader();

	romba_exit();
	return errCode;
}


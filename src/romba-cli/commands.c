
//=============================================================================
// LIBS
//=============================================================================
#include "commands.h"
#include "util.h"


//=============================================================================
// CONSTANTS
//=============================================================================

// Commands byte size
#define COMMAND_FRAME_BYTES     ENDPOINT_LENGTH

// Default time to make a bulk transfer
#define REGULAR_TIMEOUT         3000
#define CART_ERASE_TIMEOUT      70000
//#define RETRIES         3

//=============================================================================
// VARS
//=============================================================================
libusb_device_handle *mdma_handle; // The romba device handle.
libusb_device *mdma_dev;


//=============================================================================
// FUNCTION PROTOTYPES
//=============================================================================
int mdma_bulk_send_command( s8 * cmd_name, Command * command );
int mdma_bulk_get_reply_data( Command * command, u8 *buffer, u16 length, int timeout );

//=============================================================================
// FUNCTION DECLARATIONS
//=============================================================================

//-----------------------------------------------------------------------------
// MDMA_MANID_GET
//-----------------------------------------------------------------------------
u16 MDMA_manId_get()
{
	Command command_out = { { MDMA_MANID_GET } };
	Command command_in; // CMD byte + MANID word.
	int r;

	r = mdma_bulk_send_command( "MANID_GET", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;


	// Checks read info
	if( command_in.frame.cmd == MDMA_OK ) {
		u16 * code = ( u16 * ) &command_in.bytes[1];
		printf( "MANID = 0x%.4X \n", *code );
	}
	else
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );


	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_DEVID_GET
//-----------------------------------------------------------------------------
u16 MDMA_devId_get()
{
	Command command_out = { { MDMA_DEVID_GET } };
	Command command_in; // CMD byte + DEVID 3 bytes.

	int r;

	r = mdma_bulk_send_command( "DEVID_GET", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;


	// Checks read info
	if( command_in.frame.cmd == MDMA_OK ) {
		printf( "DEVID code 1 = 0x%.2X\n", command_in.bytes[1] );
		printf( "DEVID code 2 = 0x%.2X\n", command_in.bytes[2] );
		printf( "DEVID code 3 = 0x%.2X\n", command_in.bytes[3] );
	}
	else
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );


	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_READ
//-----------------------------------------------------------------------------
u16 MDMA_read( u16 bLen, int addr, u8 * data )
{
	Command command_out = { { MDMA_READ } };
	Command command_in;

	int r;

	// Write payload length
	command_out.frame.len[0] = bLen & 0xFF;
	command_out.frame.len[1] = bLen>>8;

	// Write address
	command_out.frame.addr[0] = addr & 0xFF;
	command_out.frame.addr[1] = (addr>>8) & 0xFF;
	command_out.frame.addr[2] = (addr>>16) & 0xFF;

	r = mdma_bulk_send_command( "READ", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data(&command_in, data, bLen, REGULAR_TIMEOUT);
	if( r < 0 ) return -1;

	// Checks read info
	if( command_in.frame.cmd != MDMA_OK ) {
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: could not read %d byte(s) from address 0x%.8X \n", bLen, addr );

		return -1;
	}
	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_CART_ERASE
//-----------------------------------------------------------------------------
u16 MDMA_cart_erase()
{
	Command command_out = { { MDMA_CART_ERASE } };
	Command command_in;
	int r;

	r = mdma_bulk_send_command( "CART_ERASE", &command_out );
	if( r < 0 ) return -1;

	//printf( "Erasing flash chip...\n" );

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, CART_ERASE_TIMEOUT );
	if( r < 0 ) return -1;

	// Checks read info
	if( command_in.frame.cmd == MDMA_OK ) {
	}
	else {
		printf( "\nCommand field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: flash chip was not erased \n" );
	}


	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_RANGE_ERASE
//-----------------------------------------------------------------------------
u16 MDMA_range_erase(uint32_t addr, uint32_t length) {
	Command command_out = {{MDMA_RANGE_ERASE}};
	Command command_in;
	int r;

	command_out.erase.addr[0] = addr & 0xFF;
	command_out.erase.addr[1] = (addr>>8)  & 0xFF;
	command_out.erase.addr[2] = (addr>>16) & 0xFF;
	command_out.erase.dwlen[0] = length & 0xFF;
	command_out.erase.dwlen[1] = (length>>8)  & 0xFF;
	command_out.erase.dwlen[2] = (length>>16) & 0xFF;
	command_out.erase.dwlen[3] = (length>>24) & 0xFF;

	r = mdma_bulk_send_command( "RANGE ERASE", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, CART_ERASE_TIMEOUT );
	if( r < 0 ) return -1;


	// Checks read info
	if( command_in.frame.cmd != MDMA_OK ) {
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: could not erase flash at 0x%X:%X \n", addr, length );
	}



	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_SECT_ERASE
//-----------------------------------------------------------------------------
u16 MDMA_sect_erase( int addr )
{
	Command command_out = { { MDMA_SECT_ERASE } };
	Command command_in;
	int r;

	int * addr_pointer = ( int * ) &command_out.bytes[1];
	*addr_pointer = addr;

	r = mdma_bulk_send_command( "SECT_ERASE", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;


	// Checks read info
	if( command_in.frame.cmd != MDMA_OK ) {
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: could not erase sector at 0x%.8X \n", addr );
	}


	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_WRITE
//-----------------------------------------------------------------------------
u16 MDMA_write( u16 bLen, int addr, u8 * data )
{
	Command command_out = { { MDMA_WRITE } };
	Command command_in;

	int r;
	int size;

	// Write payload length
	command_out.frame.len[0] = bLen & 0xFF;
	command_out.frame.len[1] = bLen>>8;

	// Write address
	command_out.frame.addr[0] = addr & 0xFF;
	command_out.frame.addr[1] = (addr>>8) & 0xFF;
	command_out.frame.addr[2] = (addr>>16) & 0xFF;

	r = mdma_bulk_send_command( "WRITE", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;

	if( command_in.frame.cmd == MDMA_OK ) {
		// Send big data payload
		r = libusb_bulk_transfer(mdma_handle, MeGaWiFi_ENDPOINT_OUT,
				data, bLen, &size, REGULAR_TIMEOUT);

		if (r != LIBUSB_SUCCESS && size != bLen) {
			PrintErr("Error: couldn't write payload!\n");
			PrintErr("   Code: %s\n", libusb_error_name(r) );
		}

	}
	else {
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: could not send %d byte(s) at address 0x%.8X \n", bLen, addr );

		return -1;
	}

	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_BOOTLOADER
//-----------------------------------------------------------------------------
u16 MDMA_bootloader()
{
	Command command_out = { { MDMA_BOOTLOADER } };
	int r;

	r = mdma_bulk_send_command( "BOOTLOADER", &command_out );
	if( r < 0 ) return -1;

	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_BUTTON_GET
//-----------------------------------------------------------------------------
u16 MDMA_button_get(uint8_t *button_status)
{
	Command command_out = { { MDMA_BUTTON_GET } };
	Command command_in;
	int r;

	r = mdma_bulk_send_command( "BUTTON_GET", &command_out );
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;

	*button_status = command_in.frame.len[0];

	return 0;
}

//-----------------------------------------------------------------------------
// MDMA_IO_WRITE
//-----------------------------------------------------------------------------
u16 MDMA_io_write(uint16_t addr, uint8_t data)
{
	Command command_out = { { MDMA_IO_WRITE } };
	Command command_in;
	int r;

	command_out.bytes[1] = data;
	command_out.bytes[2] = addr;
	command_out.bytes[3] = addr>>8;

	r = mdma_bulk_send_command("IO_WRITE", &command_out);
	if( r < 0 ) return -1;

	r = mdma_bulk_get_reply_data( &command_in, NULL, 0, REGULAR_TIMEOUT );
	if( r < 0 ) return -1;

	// Checks read info
	if( command_in.frame.cmd != MDMA_OK ) {
		printf( "Command field byte = 0x%.2X (MDMA_ERR) \n", command_in.frame.cmd );
		printf( "Error: could not write to IO range!\n");
	}


	return 0;
}




//-----------------------------------------------------------------------------
// MEGAWIFI_BULK_SEND_COMMAND
//-----------------------------------------------------------------------------
int mdma_bulk_send_command( s8 * cmd_name, Command * command )
{
	int ret;
	int size;

	ret = libusb_bulk_transfer( mdma_handle, MeGaWiFi_ENDPOINT_OUT,
			command->bytes, COMMAND_FRAME_BYTES, &size, REGULAR_TIMEOUT );

	if( ret != LIBUSB_SUCCESS && size != COMMAND_FRAME_BYTES )
	{
		printf( "Error: bulk transfer can not send %s command \n",
				cmd_name );

		printf( "   Code: %s\n", libusb_error_name(ret) );

		return -1;
	}

	return 0;
}

//-----------------------------------------------------------------------------
// MEGAWIFI_BULK_GET_REPLY_DATA
//-----------------------------------------------------------------------------
int mdma_bulk_get_reply_data( Command * command, u8 *buffer, u16 length, int timeout )
{
	int ret;
	int size;
	u16 recvd = 0;
	u16 step;

	// Receive the reply to the command
	ret = libusb_bulk_transfer( mdma_handle,
			MeGaWiFi_ENDPOINT_IN, command->bytes, COMMAND_FRAME_BYTES, &size, timeout );

	if( ret != LIBUSB_SUCCESS && size != COMMAND_FRAME_BYTES ) {
		printf( "Error: bulk transfer reply failed \n" );
		printf( "   Code: %s\n", libusb_error_name(ret) );
		return -1;
	}

	if (buffer && length) {
		// Now receive the big data payload
		while (recvd < length) {
			step = MIN(MAX_USB_TRANSFER_LEN, length - recvd);
			ret = libusb_bulk_transfer(mdma_handle, MeGaWiFi_ENDPOINT_IN,
					buffer+recvd, step, &size, timeout);

			if (ret != LIBUSB_SUCCESS && size != step) {
				PrintErr("Error: couldn't get read payload!\n");
				PrintErr("   Code: %s\n", libusb_error_name(ret) );
			}
			recvd += step;
		}
	}

	return 0;
}


/************************************************************************//**
 * \file
 * \brief ROM bank selection.
 *
 * This module handles ROM changes for the CPC cartridge.
 * The module takes as input the memory address desired to read/write to,
 * and writes to the ROM select register mapped at IO 0xDFXX.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 * \defgroup romsel romsel
 * \{
 * \brief ROM bank selection.
 *
 * This module handles ROM changes for the CPC cartridge.
 * The module takes as input the memory address desired to read/write to,
 * and writes to the ROM select register mapped at IO 0xDFXX.
 ****************************************************************************/

#ifndef _ROMSEL_H_

#include "cart_if.h"

/// Address of the ROM selection register in the I/O range
#define RS_ROM_SEL_ADDR		0xDF00
/// ROM selection register I/O address, higher bit
#define RS_ROM_SEL_H_ADDR	0xFC0B

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * \brief Resets bank switching logic and forces ROM bank 0 to be selected.
 *
 * This function should be called before any other in this module, to
 * perform local data initialization.
 ****************************************************************************/
void RsReset(void);

/************************************************************************//**
 * \brief Writes specified data to the indicated IO address.
 *
 * \param[in] addr Address to which to write data.
 * \param[in] data Data to write to addr.
 ****************************************************************************/
void RsIoWrite(uint16_t addr, uint8_t data);

/************************************************************************//**
 * \brief Switches to the indicated ROM bank.
 *
 * \param[in] rom_num ROM bank number (0 to 511).
 ****************************************************************************/
void RsBankRegWrite(uint16_t rom_num);

/************************************************************************//**
 * \brief Returns currently selected ROM bank.
 *
 * \return Selected ROM bank number (0 to 511).
 ****************************************************************************/
uint16_t RsGetCurrent(void);

#ifdef __cplusplus
}
#endif

#endif /* _ROMSEL_H_ */

/** \} */


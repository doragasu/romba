/************************************************************************//**
 * \file
 * \brief Flash chip management.
 *
 * This module allows to manage (mainly read and write) from/to flash
 * memory chips such as S29GL032.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/

#include "flash.h"
#include "util.h"
#include <LUFA/Drivers/Board/LEDs.h>
#include "romsel.h"

/// Obtains a sector address from an address. Current chip does not actually
//require any calculations to get SA.
/// \todo Support several flash chips
#define FLASH_SA_GET(addr)	((uint16_t)addr)

/// Number of shifts for addresses of saddr array
#define FLASH_SADDR_SHIFT	8

/// Top address of the Flash chip, plus 1, shifted FLASH_SADDR_SHIFT times.
#define FLASH_SADDR_MAX		(FLASH_CHIP_LENGTH>>FLASH_SADDR_SHIFT)

/// Sector word addresses, shifted FLASH_SADDR_SHIFTS times to the right
/// Note not all the sectors are the same length (depending on top boot
/// or bottom boot flash configuration).
const static uint16_t saddr[] = {
	// 8 MiB, Bottom Boot (S29GL064N04)
	0x0000, 0x0020, 0x0040, 0x0060, 0x0080, 0x00A0, 0x00C0, 0x00E0,
	0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700, 0x0800,
	0x0900, 0x0A00, 0x0B00, 0x0C00, 0x0D00, 0x0E00, 0x0F00, 0x1000,
	0x1100, 0x1200, 0x1300, 0x1400, 0x1500, 0x1600, 0x1700, 0x1800,
	0x1900, 0x1A00, 0x1B00, 0x1C00, 0x1D00, 0x1E00, 0x1F00, 0x2000,
	0x2100, 0x2200, 0x2300, 0x2400, 0x2500, 0x2600, 0x2700, 0x2800,
	0x2900, 0x2A00, 0x2B00, 0x2C00, 0x2D00, 0x2E00, 0x2F00, 0x3000,
	0x3100, 0x3200, 0x3300, 0x3400, 0x3500, 0x3600, 0x3700, 0x3800,
	0x3900, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 0x4000,
	0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800,
	0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00, 0x5000,
	0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800,
	0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00, 0x6000,
	0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800,
	0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00, 0x7000,
	0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800,
	0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00
	// 4 MiB, Top Boot
	//	0x0000, 0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700,
	//	0x0800, 0x0900, 0x0A00, 0x0B00, 0x0C00, 0x0D00, 0x0E00, 0x0F00,
	//	0x1000, 0x1100, 0x1200, 0x1300, 0x1400, 0x1500, 0x1600, 0x1700,
	//	0x1800, 0x1900, 0x1A00, 0x1B00, 0x1C00, 0x1D00, 0x1E00, 0x1F00,
	//	0x2000, 0x2100, 0x2200, 0x2300, 0x2400, 0x2500, 0x2600, 0x2700,
	//	0x2800, 0x2900, 0x2A00, 0x2B00, 0x2C00, 0x2D00, 0x2E00, 0x2F00,
	//	0x3000, 0x3100, 0x3200, 0x3300, 0x3400, 0x3500, 0x3600, 0x3700,
	//	0x3800, 0x3900, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00,
	//	0x3F20, 0x3F40, 0x3F60, 0x3F80, 0x3FA0, 0x3FC0, 0x3FE0
};


/// Returns the sector number corresponding to address input
#define FLASH_NSECT		(sizeof(saddr) / sizeof(uint16_t))

/**
 * Public functions
 */

uint8_t FlashDataPoll(uint16_t addr, uint8_t data)
{
	uint16_t read;

	// Poll while DQ7 != data(7) and DQ5 == 0 and DQ1 == 0
	do {
		read = FlashRead(addr);
	} while (((data ^ read) & 0x80) && ((read & 0x22) == 0));

	// DQ7 must be tested after another read, according to datasheet
	//	if (((data ^ read) & 0x80) == 0) return 0;
	read = FlashRead(addr);
	if (((data ^ read) & 0x80) == 0) return 0;
	// Data has not been programmed, return with error. If DQ5 is set, also
	// issue a reset command to return to array read mode
	if (read & 0x20) {
		FlashReset();
	}
	// If DQ1 is set, issue the write-to-buffer-abort-reset command.
	if (read & 0x02) {
		FlashUnlock();
		FlashReset();
	}
	return 1;
}

uint8_t FlashErasePoll(uint16_t addr)
{
	uint8_t read;

	// Wait until DQ7 or DQ5 are set
	do {
		read = FlashRead(addr);
	} while (!(read & 0xA0));


	// If DQ5 is set, an error has occurred. Also a reset command needs to
	// be sent to return to array read mode.
	if (!(read & 0x80)) return 0;

	FlashReset();
	return 1;
	//return (read & 0x80) != 0;
}

void FlashInit(void)
{
	RsReset();
}


void FlashIdle(void)
{
	// Control signals
	CIF_SET__WR;
	CIF_SET__RAMRD;
	CIF_SET__ROMEN;
	// Data ports (input with pullups enabled)
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
	// Addresses
	CIF_ADDRH_PORT  = CIF_ADDRL_PORT = 0xFF;
}

uint8_t FlashGetManId(void)
{
	uint16_t retVal;

	// Set bank 1
	RsBankRegWrite(1);

	// Obtain manufacturer ID and reset interface to return to array read.
	FlashAutoselect();
	retVal = FlashRead(FLASH_MANID_RD[0]);
	FlashReset();

	return retVal;
}

void FlashGetDevId(uint8_t devId[3])
{
	// Set bank 1
	RsBankRegWrite(1);

	// Obtain device ID and reset interface to return to array read.
	FlashAutoselect();
	devId[0] = FlashRead(FLASH_DEVID_RD[0]);
	devId[1] = FlashRead(FLASH_DEVID_RD[1]);
	devId[2] = FlashRead(FLASH_DEVID_RD[2]);
	FlashReset();
}

void FlashProg(uint32_t addr, uint8_t data)
{
	uint8_t i;
	uint8_t bank;

	// Obtain bank and check it is valid
	bank = addr>>14;
#ifdef _DISABLE_BANK_0
	if (0 == bank) return;
#endif
#ifdef _DISABLE_BANK_7
	if (7 == bank) return;
#endif
	// Select corresponding bank
	RsBankRegWrite(bank);
	// Unlock and write program command
	FlashUnlock();
	FLASH_WRITE_CMD(FLASH_PROG, i);
	// Write data
	FlashWrite(addr, data);
}

uint8_t FlashWriteBuf(uint32_t addr, const uint8_t data[], uint8_t bLen)
{
	// Sector address
	uint16_t sa;
	// Number of bytes to write
	uint8_t bc;
	// Index
	uint8_t i;
	// Bank to select
	uint16_t bank;

	// Check maximum write length
	if (bLen > 32) return 0;

	// Obtain bank and check it is valid
	bank = addr>>14;
#ifdef _DISABLE_BANK_0
	if (0 == bank) return;
#endif
#ifdef _DISABLE_BANK_7
	if (7 == bank) return;
#endif
	// Select corresponding bank
	RsBankRegWrite(bank);
	// Obtain the sector address
	sa = FLASH_SA_GET(addr);
	// Compute the number of words to write minus 1. Maximum number is 15,
	// but without crossing a write-buffer page
	bc = MAX(bLen, 32 - (addr & 0x1F)) - 1;
	// Unlock and send Write to Buffer command
	FlashUnlock();
	FlashWrite(sa, FLASH_WR_BUF[0]);
	// Write byte count - 1
	FlashWrite(sa, bc);

	// Write data to bufffer
	for (i = 0; i <= bc; i++, addr++) FlashWrite(addr, data[i]);
	// Write program buffer command
	FlashWrite(sa, FLASH_PRG_BUF[0]);
	// Poll until programming is complete
	if (FlashDataPoll(addr - 1, data[i - 1])) return 0;

	// Return number of elements written
	return i;
}

uint8_t FlashReadBuf(uint32_t addr, uint8_t data[], uint8_t bLen)
{
	uint16_t i;

	// Select bank
	RsBankRegWrite(addr>>14);
	// Read data
	for (i = 0; i < bLen; i++, addr++) data[i] = FlashRead(addr);

	return i;
}

void FlashUnlockBypass(void)
{
	uint8_t i;

	FlashUnlock();
	FLASH_WRITE_CMD(FLASH_UL_BYP, i);
}

void FlashUnlockBypassReset(void)
{
	// Write reset command. Addresses are don't care
	FlashWrite(0, FLASH_UL_BYP_RST[0]);
	FlashWrite(0, FLASH_UL_BYP_RST[1]);
}

uint8_t FlashChipErase(void)
{
	uint8_t i;

	// Select bank 1
	RsBankRegWrite(1);
	// Unlock and write chip erase sequence
	FlashUnlock();
	FLASH_WRITE_CMD(FLASH_CHIP_ERASE, i);
	// Poll until erase complete
	return FlashErasePoll(0);
}

uint8_t FlashSectErase(uint32_t addr)
{
	// Sector address
	uint16_t sa;
	// Index
	uint8_t i;
	uint8_t bank;

	bank = addr>>14;
#ifdef _DISABLE_BANK_0
	if (0 == bank) return;
#endif
#ifdef _DISABLE_BANK_7
	if (7 == bank) return;
#endif

	// Select bank
	RsBankRegWrite(bank);
	// Obtain the sector address
	sa = FLASH_SA_GET(addr);
	// Unlock and write sector address erase sequence
	FlashUnlock();
	FLASH_WRITE_CMD(FLASH_SEC_ERASE, i);
	// Write sector address 
	FlashWrite(sa, FLASH_SEC_ERASE_WR[0]);
	// Wait until erase starts (polling DQ3)
	while (!(FlashRead(sa) & 0x08));
	// Poll until erase complete
	return FlashErasePoll(addr);
}

uint8_t FlashRangeErase(uint32_t addr, uint32_t len)
{
	// Index
	uint8_t i, j;
	// Shifted address to compare with 
	uint16_t caddr = addr>>FLASH_SADDR_SHIFT;
	uint16_t clen = (len - 1)>>FLASH_SADDR_SHIFT;

	if (!len) return 0;
	if ((addr + len) >= (FLASH_SADDR_MAX<<FLASH_SADDR_SHIFT)) return 1;

	// Find sector containing the initial address
	for (i = FLASH_NSECT - 1; caddr < saddr[i]; i--);
	// Find sector containing the end address
	for (j = FLASH_NSECT - 1; (caddr + clen) < saddr[j]; j--);

	for (; i <= j; i++) {
		if (!FlashSectErase(((uint32_t)(saddr[i]))<<FLASH_SADDR_SHIFT)) {
			return 2;
		}
	}

	return 0;
}

/** \} */



/************************************************************************//**
 * \file
 * \brief ROM bank selection.
 *
 * This module handles ROM changes for the CPC cartridge.
 * The module takes as input the memory address desired to read/write to,
 * and writes to the ROM select register mapped at IO 0xDFXX.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/

#include "romsel.h"

/// Selected ROM bank shadow
static uint16_t bank_shadow;

/************************************************************************//**
 * \brief Resets bank switching logic and forces ROM bank 0 to be selected.
 *
 * This function should be called before any other in this module, to
 * perform local data initialization.
 ****************************************************************************/
void RsReset(void) {
    // Set bank shadow to nonzero, to enforce bank synchronization
    bank_shadow = 1;
	RsBankRegWrite(0);
}

/************************************************************************//**
 * \brief Writes specified data to the indicated IO address.
 *
 * \param[in] addr Address to which to write data.
 * \param[in] data Data to write to addr.
 ****************************************************************************/
void RsIoWrite(uint16_t addr, uint8_t data) {
    	// Set address, data and control buses
    	CIF_ADDRH_PORT = addr>>8;
    	CIF_ADDRL_PORT = addr & 0xFF;
    	CIF_DATA_PORT = data;
    	CIF_DATA_DDR = 0xFF;
    	CIF_CLR__IORQ;
    	CIF_CLR__WR;
        _NOP();
    	// Generate clock and set buses to default state
    	CIF_SET_CLK4;
    	CIF_SET__WR;
    	CIF_SET__IORQ;
    	CIF_DATA_DDR = 0x00;
    	CIF_DATA_PORT = 0xFF;
    	CIF_CLR_CLK4;
}

/************************************************************************//**
 * \brief Switches to the indicated ROM bank.
 *
 * \param[in] rom_num ROM bank number (0 to 511).
 ****************************************************************************/
void RsBankRegWrite(uint16_t rom_num) {
    // Change lower byte if needed
    if ((bank_shadow & 0xFF) != (rom_num & 0xFF)) {
        RsIoWrite(RS_ROM_SEL_ADDR, rom_num);
    }

    // Change upper bit if needed
    if ((bank_shadow>>8) != (rom_num>>8)) {
        RsIoWrite(RS_ROM_SEL_H_ADDR, rom_num>>8);
    }

    // Update shadow copy
    bank_shadow = rom_num;
}

/************************************************************************//**
 * \brief Returns currently selected ROM bank.
 *
 * \return Selected ROM bank number (0 to 511).
 ****************************************************************************/
uint16_t RsGetCurrent(void) {
    return bank_shadow;
}


/************************************************************************//**
 * \file
 * \brief System finite state machine.
 *
 * System state machine. Receives events from the cartridge and USB
 * interface, and performs the corresponding actions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 * \defgroup sys_fsm sys_fsm
 * \{
 * \brief System finite state machine.
 *
 * System state machine. Receives events from the cartridge and USB
 * interface, and performs the corresponding actions.
 ****************************************************************************/
/*
 * FSM that controls the flasher. It manages the system, and processes
 * commands received via USB interface. Each time a command is received, the
 * first received byte contains the command code. The following bytes (if
 * any), contain the data needed to complete the command.
 *
 * \warning All words and dwords sent, must use LITTLE ENDIAN order.
 */

#ifndef _SYS_FSM_H_
#define _SYS_FSM_H_

#include <stdint.h>
#include "mdma-pr.h"
#include "timers.h"

/// \brief State machine events.
enum {
    SF_EVT_NONE		=  0,	///< No event (just cycle FSM)
    SF_EVT_TIMER	=  1,	///< Timer event
    SF_EVT_CIN		=  2,	///< Cartridge inserted
    SF_EVT_COUT		=  3,	///< Cartridge removed
    SF_EVT_USB_ATT	=  4,	///< USB attached and enumerated
    SF_EVT_USB_DET	=  5,	///< USB detached
    SF_EVT_USB_ERR	=  6,	///< Error on USB interface
    SF_EVT_DIN		=  7,	///< Data reception from host
    SF_EVT_DOUT		=  8,	///< Data sent to host
    SF_EVT_SW_PRESS	=  9,	///< Button pressed
    SF_EVT_SW_REL	= 10	///< Button released
};

/// \brief Pushbutton data interpretation masks.
enum {
    SF_SW_PRESSED = 1,	///< If bit set, button is pressed.
    SF_SW_EVENT   = 2	///< If bit set, a button event occurred.
};

/// \brief Allowed state machine transitions
typedef enum {
	SF_IDLE,		///< Idle state, cartridge not inserted
	SF_STAB_WAIT,	///< Wait until cart/USB stabilizes
	SF_CART_INIT,	///< Initialize cartridge (obtain cart info)
	SF_READY,		///< System ready to parse host commands
	SF_MANID_GET,	///< Obtaining manufacturer ID
	SF_DEVID_GET,	///< Obtaining device ID
	SF_CART_READ,	///< Reading cartridge
	SF_CART_ERASE,	///< Erasing cartridge
	SF_SECT_ERASE,	///< Erasing sector
	SF_CART_PROG,	///< Programming cartridge
	SF_LINE_CTRL,	///< Manual line control
	SF_ST_MAX       ///< Total number of states
} SfStat;

/// \brief Auxiliar flags to define system status
typedef union {
	uint8_t all;				///< Access to all flags
	struct {
		uint8_t cart_in:1;		///< Cartridge inserted
		uint8_t usb_ready:1;	///< USB attached and ready
	};
} SfFlags;

/// \brief Data structure describing the cartridge flash chip.
typedef struct {
	uint8_t	manId;		///< Chip manufacturer ID
	uint8_t	devId[3];	///< Chip device ID
} SfFlashData;

/// Data structure about the currently running system instance.
typedef struct {
	SfStat s;		///< Current system status
	SfFlags f;		///< System status flags
	SfFlashData fc;	///< Flash chip data
	uint8_t sw;		///< Switch (pushbutton) status
} SfInstance;

/*
 * Public functions
 */

/************************************************************************//**
 * \brief Module initialization. Must be called before using any other function
 * from this module.
 ****************************************************************************/
void SfInit(void);

/************************************************************************//**
 * \brief Takes an incoming event and executes a cycle of the system FSM
 *
 * \param[in] evt Incoming event to be processed.
 *
 * \note Lots of states have been removed, might be needed if problems arise
 * because USB_USBTask() needs to be serviced more often than it is with
 * the current implementation.
 ****************************************************************************/
void SfFsmCycle(uint8_t evt);

/// Returns TRUE if a SF_EVT_TIMER event must be noticed to the FSM
#define SfEvtTimerNotify()	Timer1Ovfw()

#endif /*_SYS_FSM_H_*/

/** \} */


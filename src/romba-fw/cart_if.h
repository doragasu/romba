/************************************************************************//**
 * \file
 * \brief Cartridge interface.
 *
 * Contains some definitions to use some special cart lines, not used by
 * the Flash chip interface.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 * \defgroup cart_if cart_if
 * \{
 * \brief Cartridge interface.
 *
 * Contains some definitions to use some special cart lines, not used by
 * the Flash chip interface.
 ****************************************************************************/

#ifndef _CIF_H_
#define _CIF_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/cpufunc.h>

/*
 * CUSTOMIZE THE FOLLOWING DEFINITIONS TO MATCH YOUR MICROCONTROLLER WIRING
 */
/** \addtogroup CifLines
 * \brief Pin letters and numbers used by each function.
 *
 * Can be customized to match the microcontroller wiring.
 * \{
 */
/// Letter used for the address bus, high (9~16) lines lines
#define CIF_ADDRH		F
/// Letter used for the address bus, lower (1~8) lines
#define CIF_ADDRL		A

/// Letter used for the data bus
#define CIF_DATA		C

/// Letter used for the active low write request
#define CIF__WR_LET		D
/// Pin number used for the active low write request
#define CIF__WR			4

/// Letter used for the active low read request
#define CIF__RD_LET		D
/// Pin number used for the active low read request
#define CIF__RD			5

/// Letter used for the active low IO request
#define CIF__IORQ_LET	D
/// Pin number used for the active low IO request
#define CIF__IORQ		6

/// Letter used for the active low IO request
#define CIF__MREQ_LET	E
/// Pin number used for the active low IO request
#define CIF__MREQ		0

/// Letter used for the active low RAM read request
#define CIF__RAMRD_LET	B
/// Pin number used for the active low RAM read request
#define CIF__RAMRD		6

/// Letter for ROM Enable output (active low)
#define CIF__ROMEN_LET	E
/// Pin number for ROM Enable output (ative low)
#define CIF__ROMEN		4

/// Letter for ROM disable input
#define CIF_ROMDIS_LET	B
/// Pin number for ROM disable input
#define CIF_ROMDIS		7

/// Letter for RAM disable input
#define CIF_RAMDIS_LET	B
/// Pin number for RAM disable input
#define CIF_RAMDIS		5

/// Letter for Ready signal output
#define CIF_READY_LET	D
/// Pin number for Ready signal output
#define CIF_READY		0

/// Letter for Bus request input (active low)
#define CIF__BUSRQ_LET	D
/// Pin number for Bus request output (active low)
#define CIF__BUSRQ		2

/// Letter for Bus ACK output (active low)
#define CIF__BUSAK_LET	D
/// Pin number for Bus ACK output (active low)
#define CIF__BUSAK		1

/// Letter for CPU M1 output (active low)
#define CIF__M1_LET		E
/// Pin number for CPU M1 output (active low)
#define CIF__M1			1

/// Letter for CPU Refresh output (active low)
#define CIF__RFSH_LET	D
/// Pin number for CPU Refresh output (active low)
#define CIF__RFSH		7

/// Letter for Interrupt request input (active low)
#define CIF__INT_LET	D
/// Pin number for Interrupt request input (active low)
#define CIF__INT		3

/// Cart reset (active low) letter
#define CIF__RSET_LET	E
/// Cart reset (active low) number
#define CIF__RSET		5

/// Expansion input (active low) letter
#define CIF__EXP_LET	B
/// Expansion input (Active low) number
#define CIF__EXP		4

/// Clock output letter
#define CIF_CLK4_LET	E
/// Clock output numbe
#define CIF_CLK4		3
/** \} */

/*
 * DO NOT CUSTOMIZE ANYTHING BEYOND THIS POINT.
 */

/// \brief Disables JTAG if x is true.
#define jtd_set(x) \
{ \
	__asm__ __volatile__ ( \
			"in __tmp_reg__,__SREG__" "\n\t" \
			"cli" "\n\t" \
			"out %1, %0" "\n\t" \
			"out __SREG__, __tmp_reg__" "\n\t"\
			"out %1, %0" "\n\t" \
			: /* no outputs */ \
			: "r" ((uint8_t)(x ? (1<<JTD) : 0)), \
			"M" (_SFR_IO_ADDR(MCUCR)) \
			: "r0"); \
}

/** \addtogroup BusCtrl
 * \brief Registers used for controlling the data buses.
 * \{
 */
/// Helper macro to construct port registers, second level
#define CIF_REG2(letter, reg)	(reg ## letter)
/// Helper macro to construct port registers
#define CIF_REG(letter, reg) CIF_REG2(letter, reg)

/// Address port, high bits
#define CIF_ADDRH_PORT	CIF_REG(CIF_ADDRH, PORT)
/// Address port, low bits
#define CIF_ADDRL_PORT	CIF_REG(CIF_ADDRL, PORT)

/// Address data directon register, high bits
#define CIF_ADDRH_DDR		CIF_REG(CIF_ADDRH, DDR)
/// Address data directon register, low bits
#define CIF_ADDRL_DDR		CIF_REG(CIF_ADDRL, DDR)

/// Address pin register, high bits
#define CIF_ADDRH_PIN		CIF_REG(CIF_ADDRH, PIN)
/// Address pin register, low bits
#define CIF_ADDRL_PIN		CIF_REG(CIF_ADDRL, PIN)

/// Data port
#define CIF_DATA_PORT	CIF_REG(CIF_DATA, PORT)

/// Data data direction register, high bits
#define CIF_DATA_DDR		CIF_REG(CIF_DATA, DDR)

/// Data pin register, high bits
#define CIF_DATA_PIN		CIF_REG(CIF_DATA, PIN)

/// Active low write signal port
#define CIF__WR_PORT		CIF_REG(CIF__WR_LET, PORT)
/// Active low write signal pin register
#define CIF__WR_PIN		CIF_REG(CIF__WR_LET, PIN)
/// Active low write signal data direction register
#define CIF__WR_DDR		CIF_REG(CIF__WR_LET, DDR)

/// Active low read signal port
#define CIF__RD_PORT		CIF_REG(CIF__RD_LET, PORT)
/// Active low read signal pin register
#define CIF__RD_PIN		CIF_REG(CIF__RD_LET, PIN)
/// Active low read signal data direction register
#define CIF__RD_DDR		CIF_REG(CIF__RD_LET, DDR)

/// Active low IO request signal port
#define CIF__IORQ_PORT		CIF_REG(CIF__IORQ_LET, PORT)
/// Active low IO request signal pin
#define CIF__IORQ_PIN		CIF_REG(CIF__IORQ_LET, PIN)
/// Active low IO request signal data direction
#define CIF__IORQ_DDR		CIF_REG(CIF__IORQ_LET, DDR)

/// Active low memory request signal port
#define CIF__MREQ_PORT		CIF_REG(CIF__MREQ_LET, PORT)
/// Active low memory request signal pin
#define CIF__MREQ_PIN		CIF_REG(CIF__MREQ_LET, PIN)
/// Active low memory request signal data direction
#define CIF__MREQ_DDR		CIF_REG(CIF__MREQ_LET, DDR)

/// Active low RAM read signal port
#define CIF__RAMRD_PORT		CIF_REG(CIF__RAMRD_LET, PORT)
/// Active low RAM read signal pin
#define CIF__RAMRD_PIN		CIF_REG(CIF__RAMRD_LET, PIN)
/// Active low RAM read data direction
#define CIF__RAMRD_DDR		CIF_REG(CIF__RAMRD_LET, DDR)

/// Active low ROM enable signal port
#define CIF__ROMEN_PORT		CIF_REG(CIF__ROMEN_LET, PORT)
/// Active low ROM enable pin
#define CIF__ROMEN_PIN		CIF_REG(CIF__ROMEN_LET, PIN)
/// Active low ROM enable data direction
#define CIF__ROMEN_DDR		CIF_REG(CIF__ROMEN_LET, DDR)

/// ROM disable signal port
#define CIF_ROMDIS_PORT		CIF_REG(CIF_ROMDIS_LET, PORT)
/// ROM disable pin
#define CIF_ROMDIS_PIN		CIF_REG(CIF_ROMDIS_LET, PIN)
/// ROM disable data direction
#define CIF_ROMDIS_DDR		CIF_REG(CIF_ROMDIS_LET, DDR)

/// RAM disable signal port
#define CIF_RAMDIS_PORT		CIF_REG(CIF_RAMDIS_LET, PORT)
/// RAM disable pin
#define CIF_RAMDIS_PIN		CIF_REG(CIF_RAMDIS_LET, PIN)
/// RAM disable data direction
#define CIF_RAMDIS_DDR		CIF_REG(CIF_RAMDIS_LET, DDR)

/// READY signal port
#define CIF_READY_PORT		CIF_REG(CIF_READY_LET, PORT)
/// READY pin
#define CIF_READY_PIN		CIF_REG(CIF_READY_LET, PIN)
/// READY data direction
#define CIF_READY_DDR		CIF_REG(CIF_READY_LET, DDR)

/// Active low Bus Request port
#define CIF__BUSRQ_PORT		CIF_REG(CIF__BUSRQ_LET, PORT)
/// Active low Bus Request pin
#define CIF__BUSRQ_PIN		CIF_REG(CIF__BUSRQ_LET, PIN)
/// Active low Bus Request data direction register
#define CIF__BUSRQ_DDR		CIF_REG(CIF__BUSRQ_LET, DDR)

/// Active low Bus acknowledge port
#define CIF__BUSAK_PORT		CIF_REG(CIF__BUSAK_LET, PORT)
/// Active low Bus acknowledge pin
#define CIF__BUSAK_PIN		CIF_REG(CIF__BUSAK_LET, PIN)
/// Active low Bus acknowledge data direction register
#define CIF__BUSAK_DDR		CIF_REG(CIF__BUSAK_LET, DDR)

/// Active low M1 port
#define CIF__M1_PORT		CIF_REG(CIF__M1_LET, PORT)
/// Active low M1 pin
#define CIF__M1_PIN		CIF_REG(CIF__M1_LET, PIN)
/// Active low M1 data direction register
#define CIF__M1_DDR		CIF_REG(CIF__M1_LET, DDR)

/// Active low refresh port
#define CIF__RFSH_PORT		CIF_REG(CIF__RFSH_LET, PORT)
/// Active low refresh pin
#define CIF__RFSH_PIN		CIF_REG(CIF__RFSH_LET, PIN)
/// Active low refresh data direction register
#define CIF__RFSH_DDR		CIF_REG(CIF__RFSH_LET, DDR)

/// Active low interrupt port
#define CIF__INT_PORT		CIF_REG(CIF__INT_LET, PORT)
/// Active low interrupt pin
#define CIF__INT_PIN		CIF_REG(CIF__INT_LET, PIN)
/// Active low interrupt data direction register
#define CIF__INT_DDR		CIF_REG(CIF__INT_LET, DDR)

/// Active low reset port
#define CIF__RSET_PORT		CIF_REG(CIF__RSET_LET, PORT)
/// Active low reset pin
#define CIF__RSET_PIN		CIF_REG(CIF__RSET_LET, PIN)
/// Active low Bus data direction register
#define CIF__RSET_DDR		CIF_REG(CIF__RSET_LET, DDR)

/// Active low EXP port
#define CIF__EXP_PORT		CIF_REG(CIF__EXP_LET, PORT)
/// Active low EXP pin
#define CIF__EXP_PIN		CIF_REG(CIF__EXP_LET, PIN)
/// Active low EXP data direction register
#define CIF__EXP_DDR		CIF_REG(CIF__EXP_LET, DDR)

/// Active low clock port
#define CIF_CLK4_PORT		CIF_REG(CIF_CLK4_LET, PORT)
/// Active low clock pin
#define CIF_CLK4_PIN		CIF_REG(CIF_CLK4_LET, PIN)
/// Active low clock data direction register
#define CIF_CLK4_DDR		CIF_REG(CIF_CLK4_LET, DDR)
/** \} */

/** \addtogroup BusSetClr 
 * \brief Macros to set and clear bus lines.
 * \{
 */
/// Set (1) active low write signal.
#define CIF_SET__WR	do{(CIF__WR_PORT |=  (1<<CIF__WR));}while(0)
/// Clear (0) active low write signal.
#define CIF_CLR__WR	do{(CIF__WR_PORT &= ~(1<<CIF__WR));}while(0)
/// Set (1) active low read signal.
#define CIF_SET__RD	do{(CIF__RD_PORT |=  (1<<CIF__RD));}while(0)
/// Clear (0) active low output enable signal.
#define CIF_CLR__RD	do{(CIF__RD_PORT &= ~(1<<CIF__RD));}while(0)
/// Set (1) active low IO request signal.
#define CIF_SET__IORQ	do{(CIF__IORQ_PORT |=  (1<<CIF__IORQ));}while(0)
/// Clear (0) active low IO request signal.
#define CIF_CLR__IORQ	do{(CIF__IORQ_PORT &= ~(1<<CIF__IORQ));}while(0)
/// Set (1) active low memory request signal.
#define CIF_SET__MREQ	do{(CIF__MREQ_PORT |=  (1<<CIF__MREQ));}while(0)
/// Clear (0) active low memory request signal.
#define CIF_CLR__MREQ	do{(CIF__MREQ_PORT &= ~(1<<CIF__MREQ));}while(0)
/// Set (1) active low RAM read signal.
#define CIF_SET__RAMRD	do{(CIF__RAMRD_PORT |=  (1<<CIF__RAMRD));}while(0)
/// Clear (0) active low RAM read signal.
#define CIF_CLR__RAMRD	do{(CIF__RAMRD_PORT &= ~(1<<CIF__RAMRD));}while(0)
/// Set (1) the active low ROM enable signal.
#define CIF_SET__ROMEN	do{CIF__ROMEN_PORT |=  (1<<CIF__ROMEN);}while(0)
/// Clear (0) the active low ROM enable signal.
#define CIF_CLR__ROMEN	do{CIF__ROMEN_PORT &= ~(1<<CIF__ROMEN);}while(0)
/// Set (1) the READY signal.
#define CIF_SET_READY	do{CIF_READY_PORT |=  (1<<CIF_READY);}while(0)
/// Clear (0) the READY signal.
#define CIF_CLR_READY	do{CIF_READY_PORT &= ~(1<<CIF_READY);}while(0)
/// Set (1) the active low bus acknowledge signal
#define CIF_SET__BUSAK	do{CIF__BUSAK_PORT |=  (1<<CIF__BUSAK);}while(0)
/// Clear (0) the active low bus acknowledge signal
#define CIF_CLR__BUSAK	do{CIF__BUSAK_PORT &= ~(1<<CIF__BUSAK);}while(0)
/// Set (1) the active low M1 signal
#define CIF_SET__M1	do{CIF__M1_PORT |=  (1<<CIF__M1);}while(0)
/// Clear (0) the active low M1 signal
#define CIF_CLR__M1	do{CIF__M1_PORT &= ~(1<<CIF__M1);}while(0)
/// Set (1) the active low reset signal.
#define CIF_SET__RSET	do{CIF__RSET_PORT |=  (1<<CIF__RSET);}while(0)
/// Clear (0) the active low reset pin.
#define CIF_CLR__RSET	do{CIF__RSET_PORT &= ~(1<<CIF__RSET);}while(0)
/// Set (1) the active low refresh pin.
#define CIF_SET__RFSH	do{CIF__RFSH_PORT |=  (1<<CIF__RFSH);}while(0)
/// Clear (0) the active low refresh pin.
#define CIF_CLR__RFSH	do{CIF__RFSH_PORT &= ~(1<<CIF__RFSH);}while(0)
/// Set (1) the clock
#define CIF_SET_CLK4	do{CIF_CLK4_PORT |=  (1<<CIF_CLK4);}while(0)
/// Clear (0) the clock
#define CIF_CLR_CLK4	do{CIF_CLK4_PORT &= ~(1<<CIF_CLK4);}while(0)


/// Returns TRUE if ROM disabled by cart, FALSE otherwise
#define CIF_ROMDIS_GET	((CIF_ROMDIS_PIN & (1<<CIF_ROMDIS)) != 0)

/// Returns TRUE if RAM disabled by cart, FALSE otherwise
#define CIF_RAMDIS_GET	((CIF_RAMDIS_PIN & (1<<CIF_RAMDIS)) != 0)

/// Returns 0 if RAM cart requests the bus, 1 otherwise
#define CIF_BUSRQ_GET	((CIF_BUSRQ_PIN & (1<<CIF_BUSRQ)) != 0)

/// Returns 0 if cart requests an interrupt, 1 otherwise
#define CIF_INT_GET	((CIF_INT_PIN & (1<<CIF_INT)) != 0)

/// Returns TRUE if cart set, FALSE otherwise
#define CIF__EXP_GET	((CIF__EXP_PIN & (1<<CIF__EXP)) != 0)
/** \} */


/************************************************************************//**
 * \brief Initializes the module. Must be called before using any other
 * macro.
 ****************************************************************************/
static inline void CifInit(void) {
	// Ensure JTAG interface is disabled to allow using PF[4~7] GPIO pins
	jtd_set(true);

	// Set RST active
	CIF__RSET_DDR  |= 1<<CIF__RSET;
	CIF_CLR__RSET;

	// Configure control lines, and set outputs to inactive
	CIF__WR_DDR    |= (1<<CIF__WR);
	CIF__RD_DDR    |= (1<<CIF__RD);
	CIF__IORQ_DDR  |= (1<<CIF__IORQ);
	CIF__MREQ_DDR  |= (1<<CIF__MREQ);
	CIF__ROMEN_DDR |= (1<<CIF__ROMEN);
	CIF__RAMRD_DDR |= (1<<CIF__RAMRD);
	CIF_CLK4_DDR   |= (1<<CIF_CLK4);
	CIF_READY_DDR  |= (1<<CIF_READY);
	CIF__BUSAK_DDR |= (1<<CIF__BUSAK);
	CIF__RFSH_DDR  |= (1<<CIF__RFSH);
	CIF__M1_DDR    |= (1<<CIF__M1);
	CIF_SET__WR;
	CIF_SET__RD;
	CIF_SET__IORQ;
	CIF_SET__MREQ;
	CIF_SET__ROMEN;
	CIF_SET__RAMRD;
	CIF_SET_CLK4;
	CIF_CLR_READY;
	CIF_SET__BUSAK;
	CIF_SET__RFSH;
	CIF_SET__M1;

	// Configure address registers as output
	CIF_ADDRH_DDR   = CIF_ADDRL_DDR = 0xFF;
	CIF_ADDRH_PORT  = CIF_ADDRL_PORT = 0xFF;

	// Configure data registers as input (with pull-ups enabled)
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;

	// Initialize inputs
	CIF__EXP_DDR     &= ~(1<<CIF__EXP);
	CIF__EXP_PORT    |=  (1<<CIF__EXP);
	CIF_ROMDIS_DDR   &= ~(1<<CIF_ROMDIS);
	CIF_ROMDIS_PORT  |=  (1<<CIF_ROMDIS);
	CIF_RAMDIS_DDR   &= ~(1<<CIF_RAMDIS);
	CIF_RAMDIS_PORT  |=  (1<<CIF_RAMDIS);
	CIF__BUSRQ_DDR   &= ~(1<<CIF__BUSRQ);
	CIF__BUSRQ_PORT  |=  (1<<CIF__BUSRQ);
	CIF__INT_DDR     &= ~(1<<CIF__INT);
	CIF__INT_PORT    |=  (1<<CIF__INT);
}

#endif /*_CIF_H_*/

/** \} */


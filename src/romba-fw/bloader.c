/************************************************************************//**
 * \file
 * \brief This module allows invoking the MDMA bootloader.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/
#include "bloader.h"
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Drivers/Board/LEDs.h>

void JumpToBootloader(void)
{
	// Disables USB and interrupts, and then enters bootloader
	LEDs_TurnOffLEDs(LEDS_ALL_LEDS);
	USB_Disable();
	cli();
	Delay_MS(2000);
	((void (*)(void))BOOTLOADER_START_ADDRESS)();
}



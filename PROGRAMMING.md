# Introduction

This document describes how to use a ROMBA cartridge from a developer perspective. It is intended to be read by programmers wishing to take advantage of the ROMBA cartridge features. First the memory map of the cartridge and the registers is shown. Then the document explains how to switch ROM banks and to perform DMA operations to copy data and play audio samples. Finally some tips are explained.

# Memory Map

ROMBA cartridges have up to 64 Megabits (8 Megabytes) of Flash ROM memory. This amount of ROM going from address $000000 to $7FFFFF is exposed to the Amstrad CPC CPU by mapping 16 KiB memory blocks that can be read in the range $0000 ~ $3FFF (Lower ROM) and $C000 ~ $FFFF. The following table shows how ROMBA cartridges map their internal Flash ROM into the CPC main memory:

| Memory range  | ROMBA Flash ROM mapping                                              |
|---------------|----------------------------------------------------------------------|
| $0000 ~ $3FFF | Reads in this area are mapped to Flash slot 7 ($1C000 ~ $1FFFF)      |
| $C000 ~ $FFFF | Reads in this area are mapped to the slot selected by `RSELH:RSELL`¹ |

1. To access slots $007 and $107, you have to enable `TRANSPARENT` bit on the `CONF` register (see [CONF register](#conf-register) below). By default these slots cannot be accessed to avoid conflicts with the DOS ROM on CPC 6128 machines.

The following table shows the ROMBA internal registers mapped to the I/O range. Keep in mind these registers are write-only:

| I/O address | Register | Description                        |
|-------------|----------|------------------------------------|
|   $FC00     | DMASRCL  | DMA source address, low byte       |
|   $FC01     | DMASRCH  | DMA source address, high byte      |
|   $FC02     | DMASRCU  | DMA source address, upper byte     |
|   $FC04     | DMADSTL  | DMA destination address, low byte  |
|   $FC05     | DMADSTH  | DMA destination address, high byte |
|   $FC06     | DMALENL  | DMA transfer length, low byte      |
|   $FC07     | DMALENH  | DMA transfer length, high byte     |
|   $FC08     | DMACTRL  | DMA control register               |
|   $FC09     | DMACDIV  | DMA clock divisor                  |
|   $FC0A     | DACOUT   | DAC output register                |
|   $FC0B     | RSELH    | ROM bank selection, higher bit     |
|   $FC0C     | CONF     | Configuration register             |
|   $DFXX     | RSELL    | ROM bank selection, lower byte     |

The function of each register should be clear by reading its description. The only registers requiring further explanation should be the `DMACTRL` and `CONF` registers that are documented in the following subsections

## DMACTRL register

This is the DMA control register. It is formatted as follows:

| #Bit  | Function | Description                                                  |
|-------|----------|--------------------------------------------------------------|
| 0     | START    | Write '1' to this bit to start a DMA transfer                |
| 1     | SRCINC   | ROM to RAM source address auto-increment. '1' to enable      |
| 2     | DSTINC   | ROM to RAM destination address auto-increment. '1' to enable |
| 3 ~ 5 | RESERVED | Reserved, always write 0 to these bits                       |
| 6     | FULLADDR | Full addressing enable when '1'                              |
| 7     | MODE     | DMA mode                                                     |

The `FULLADDR` bit (6) configures the Flash ROM addressing mode:
* '0': Full addressing disabled, use the currently configured 16 KiB bank for the DMA transfer.
* '1': Full addressing enabled, use the full 23-bit addressing range configured by the `DMASRCU:DMASRCH:DMASRCL` registers.

The MODE bit (7) configures the DMA mode:

* '0': Mode 0, ROM to RAM DMA. Copy data from the ROMBA Flash ROM to the CPC RAM.
* '1': Mode 1, ROM to DAC. Copy data from the ROMBA Flash ROM to the DAC register at regular intervals.

## CONF register

This is the configuration register. Currently it only has two bits, defined as follows:

| #Bit  | Function    | Description                                                     |
|-------|-------------|-----------------------------------------------------------------|
| 0     | TRANSPARENT | Write '1' to this bit to configure transparent addressing mode  |
| 1     | WRITE\_EN   | When set to '1', writes to upper ROM are sent to the flash chip |
| 2 ~ 7 | RESERVED    | Reserved, always write 0 to these bits                          |

`TRANSPARENT` bit is disabled after reset. When disabled, reads to ROM slots $007 and $107 in the memory range $C000 ~ $FFFF are ignored to avoid conflicts with CPC 6128 DOS ROM, that cannot be overridden. When `TRANSPARENT` is set to '1', transparent addressing is enabled, and reads to ROM slots 7 and $107 in the range $C000 ~ $FFFF are honored.

__Warning__: transparent mode should not be enabled on CPC 6128 machines unless you know what you are doing. Enabling this mode on a CPC 6128 can cause garbage to be read when $007 and $107 slots are selected, leading to data corruption and crashes.

`WRITE_EN` is disabled after reset. When this bit is disabled, it is not possible to write to the Flash chip. When enabled (set to '1'), writes to the $C000 ~ $FFFF range are applied to the Flash chip in the cartridge. __Note__: these writes *also* affect the RAM segment configured in this zone (that by default is the screen memory).

# Using the cartridge

## Selecting the 16 KiB ROM slot

If we set aside the extra 256 slots (selected by writing '1' to `RSELH`), ROMBA is 100% compatible with standard ROM boxes. Thus the 16 KiB ROM bank mapped to the $C000 to $FFFF range can be changed the same, by writing the desired slot (from 0 to 255) to `RSELL` in the $DF00 to $DFFF range (or more exactly to the $DC00 to $DFFF range).

But you can select 256 additional banks by writing '1' to `RSELH`. Or to put it simple, the selected slot is configured using the 9 bits resulting when you join the `RSELH:RSELL` registers. For example selecting slot 0, maps the Flash ROM range $000000 ~ $003FFF in the CPU memory range $C000 ~ $FFFF. Selecting slot 1 maps the Flash ROM range $004000 ~ $007FFF into the same CPU memory range, and so on, up to slot 511, that maps Flash ROM range $7FC000 ~ $7FFFFF. There is an exception to this rule with slots 7 and $107, as you can read below.

## Using slots $007 and $107

As previously indicated in this document, slots 7 and $107 cannot be mapped to the $C000 ~ $FFFF range to avoid conflicts with the DOS ROM in CPC 6128 machines. If you select any of these slots, accessing the range $C000 ~ $FFFF will return data from the DOS ROM (if installed) instead of the ROM in the ROMBA cartridge. Anyway, slot 7 is always mapped in the Lower ROM ($0000 to $3FFF), so you should not need mapping it to the $C000 ~ $FFFF range, and the data in both slots can also be perfectly accessed using DMA. So to avoid losing 16 KiB of valuable data in slot $107, you can put there for example audio samples, that can be perfectly played using the DMA engine.

## Copying data from Flash ROM to RAM using DMA

DMA can be used in mode 0 to copy data from the ROMBA Flash ROM to the CPC RAM at high speed. **When doing ROM to RAM DMA transfers, the CPU is halted prior to starting the DMA, and resumed when DMA transfer ends**. Although the CPU is halted during the process, copying data using DMA is much faster than doing it using the CPU: DMA can copy 1 byte per microsecond, while using a typical CPU loop with the `ldi` instruction takes 5 microseconds per byte.

An additional advantage of using DMA, is that the controller can copy data from the currently selected ROM slot, or from the entire 23-bit Flash ROM memory range. Thus you do not need to change the selected ROM bank to copy data from a different one.

Each time the DMA controller copies one byte, it can optionally increase the configured source and destination addresses (by enabling the `SRCINC` and `DSTINC` bits). This is typically the desired behavior, but there may be some cases in which you might not want to auto-increment an address. For example if you enable `DSTINC` but disable `SRCINC`, you can perform fast fill operations: point the source address to a position with a `0` value, and you can fill with zeros very fast a memory area of your choice.

The steps you have to follow to start a mode 0 ROM to RAM DMA transfer are as follows:

1. Set the source address using `DMASRCL`, `DMASRCH` and, if using full-addressing, `DMASRCU` registers.
2. Set the destination address using `DMADSTL` and `DMADSTH` registers.
3. Set the transfer length using `DMALENL` and `DMALENH` registers.
4. Start the DMA transfer by writing the control byte to the `DMACTRL` register, with the `START` bit (0) set to '1'. Typically you will be writing $47 to this register to start a DMA transfer in full-addressing mode, or $07 to start a DMA transfer with full-addressing disabled.

**NOTE**: When the DMA transfer ends, `DMALENH:DMALENL` registers will be zeroed. The address registers (`DMASRCL`, `DMASRCH`, `DMASRCU`, `DMADSTL` and `DMADSTH`) will have the value corresponding to the last address that the DMA controller has accessed (the same that was configured if auto-increment was disabled, or the one configured plus the original transfer length, if auto-increment was enabled).

## Playing digital audio samples

ROMBA cartridge has an additional 8-bit digital audio channel. To play a sound, the samples must be written at regular intervals to the `DACOUT` (Digital to Analog Converter output) register. In addition to being 8-bit length, samples must be unsigned and the center point must be at $80. You could use a CPU routine to write the samples, but this can be pretty CPU intensive. Fortunately you can also use the DMA mode 1 to play the samples for you without CPU intervention. In this mode, the DMA controller reads samples from the Flash ROM at regular intervals, and writes them to the `DACOUT` register. Note that in contrast to DMA mode 0, in DMA mode 1, CPU is not halted. Thus digital audio samples can be played without disturbing the CPU at all.

While DMA mode 1 is active, the CPU can also access the Flash ROM without a problem, exactly the same as if the DMA was not active. But **while DMA mode 1 is active, you must be careful not to write to the following DMA registers: `DMASRCL`, `DMASRCH`, `DMASRCU`, `DMALENL`, `DMALENH`, `DMACTRL` and `DMACDIV`**. Failing to comply with this restriction might cause undefined behavior. As a direct consequence of this restriction, you cannot start a new DMA transfer while another is in progress.

During ROM to DAC DMA transfers, the sampling rate `Fs` is determined by the value configured in `DMACDIV` by using the following expression:

```
Fs (kHz) = 1000 / (DMACDIV + 1)
```

Setting for example a value of 255 in `DMACDIV`, configures a sampling rate of 3.9 kHz. As the maximum DMA transfer length is limited to 65535 bytes, the sampling rate also determines the maximum duration of the played sample.  Some typical values you might want to use are presented in the following table:

| `DMACDIV` value | Sampling rate (kHz) | Maximum duration (s) |
|-----------------|---------------------|----------------------|
| 124             | 8                   | 8.1                  |
| 61              | 16.1                | 4                    |
| 49              | 20                  | 3.2                  |
| 22              | 43.5                | 1.5                  |
| 20              | 47.6                | 1.3                  |
| 19              | 50                  | 1.3                  |

To start a mode 1 ROM to DAC DMA transfer, follow these steps:

1. Configure `DMACDIV` to obtain the desired sampling rate. NOTE: Unless you need to change the sampling rate for different samples, this step needs to be done only once, you can skip it if done previously.
2. Configure the source address registers `DMASRCL`, `DMASRCH` and `DMASRCU`.
3. Configure the transfer length using `DMALENL` and `DMALENH` registers.
4. Start the transfer by writing the control byte to `DMACTRL` register. Typically you will write $C3 to this register to start a ROM to DAC transfer.

Note that currently there is no way to ask the system if it has ended playing a sample. I might try adding an option to enable notifications via NMI, but it will be difficult making it fit in the CPLD.

## Writing to Flash from the CPC

Flash can be written from the CPC itself, but there are some restrictions you must keep in mind when writing to flash:

* To program (write) data to the flash chip, the memory address you are writing to must have been previously erased (it should have value $FF).
* The upper ROM must be enabled to issue commands to the flash chip. Commands are sent to the $C000 ~ $FFFF memory range.
* Flash erase operation is done with sector granularity. You cannot erase a single byte of data, a complete sector must be erased in its entirety.
* Typically the flash chip installed in the cartridge will be a `S29GL032` or `S29GL064`. In these chips, sector size ranges from 8 KiB to 64 KiB.
* Code dealing with the Flash chip must be located in RAM. Issuing commands to the flash chip from ROM will most likely crash the machine.
* Writes to the flash chip are mapped at the range $C000 ~ $FFFF. When you enable flash writes, any write to this range will reach the flash chip. But __it will simultaneously reach the RAM mapped to this zone__ (that is typically the screen RAM). So yes, if you have the screen RAM there, you will most likely see garbage on the screen when you write to flash. Sorry about that.

Even with these limitations, the ability to write to flash can be useful for example to save game progress and scores. The fact that flash sector lengths are this big is inconvenient, but as there is plenty space available, it should not be a problem to spare most of the sector you are using to save. For example, if you want to store a table with the top 10 scores as 32 bit variables, along with the player names with up to 12 characters, that will take `10 * (12 + 4) = 160 octets`. Add for example a 2-byte checksum and that would make 162 octets. When you need to save a new high score, you have to keep the entire 162 bytes structure in RAM, erase the sector (it is recommended to choose an 8 KiB one) and then write the 162 bytes to the sector. You will be wasting most of the data, but wasting 8 KiB in an 8 MiB long chip should not be a problem.

To be able to send commands to the flash chip, you must enable flash writes, by setting bit `WRITE_EN` in the `CONF` register. Once you finish writing to the flash chip, it is recommended to clear this bit to avoid writes to the RAM mapped in the $C000 ~ $FFFF area reaching the flash chip.

More advanced techniques can take advantage of the fact that on the contrary to what I wrote above, you can program (write) to memory addresses that are not erased (have not value $FF), as long as the write operation only attempts to change ones to zeros. E.g. the following writes are allowed and will succeed:

* Write $00 to a memory position with value $01
* Write $3F to a memory position with value $7F
* Write $F0 to a memory position with value $F1

But the following writes __will fail__ because you will be trying to set to one bits that are zeroed:

* Write $01 to a memory position with value $00
* Write $7F to a memory position with value $3F
* Write $F1 to a memory position with value $F0

Taking advantage of this, you can implement for example several copies of the data on the same sector, clearing one flag each time you write a new updated copy at the end of the last one you wrote. Or for example you can have two sectors to first write the data to the unused one, and when finished, mark it as active.

If you use the flash to save game data, it is always recommended to verify the data integrity using any kind of checksum function, and also having an option in the game to clear the data.

To learn how to issue commands to the flash chip, you can read its datasheet. Alternatively, you can browse the driver I wrote in the `flash` module of the `romba-fw` subproject.

## Other Tips

1. Writing to `RSELH` should be rarely done. You will not need it unless your ROM uses more than 32 Megabits (difficult for an Amstrad CPC game to fill that much space), and even if you use more, you can put in the upper 32 Megabits audio samples and other data that you plan to DMA (so you will not have to select the ROM slot to access the data).
2. Maybe you can avoid some writes to DMA registers if you take into account the values each register holds when a DMA transfer has been completed.
3. If you read carefully this document, maybe you have noticed that DMA Mode 0 uses 16 bits for the destination addressing. So how can you transfer data to the whole 128 KiB RAM of a CPC 6128? You guessed right: you have to configure the RAM banking properly before starting the DMA, and then make sure the DMA destination address points to the mapped bank you want to write. Unfortunately, the CPC expansion port has not available all the signals that would be required to access the RAM chips without going through the banking mechanism.
4. If you use the feature that maps Flash ROM slot 7 to reads in the $0000 ~ $3FFF range, the CPC will boot your program directly, without running neither internal firmware routines nor the Basic ROM code. This means that booting to your game is blazingly fast, but requires you to initialize the CPC peripherals. @augurui made some code to handle this initialization (and to test all the ROMBA features). I think it will be easy to convince him to release the code 😉 so stay tuned!

# Annex A: updating the CPLD firmware

To write the firmware blob to the CPLD, you can use any programmer supporting Lattice chips and the Lattice Diamond/Lattice Diamond Programmer software. For this task I use a good old FT2232H board I bought on Aliexpress and a custom cable to connect to the ROMBA cartridge JTAG connector. I even made [a cool case for it](https://www.printables.com/model/793227-ft2232h-development-board-enclosure) and a [Gridfinity storage block](https://www.printables.com/model/793244-stackable-gridfinity-holder-for-ft2232h-programmer) for the programmer and the cables! Any FT2232H board that exposes the ADBUS0 to ADBUS3 lines should work. Cheaper variants of the FT2232H chip (such as the FT232R) should also work, but as I have not tested them, I would recommend going for the FT2232H/FT2232HL to avoid problems.

The cable you need to solder has 5 mandatory wires plus an optional sixth one:

| ROMBA JTAG pin | FT2232H pin      |
|----------------|------------------|
| 1 (TMS)        | ADBUS3           |
| 2 (TCK)        | ADBUS0           |
| 3 (TDI)        | ADBUS1           |
| 4 (TDO)        | ADBUS2           |
| 5 (GND)        | GND              |
| 6 (3.3 V)      | 3.3 V (optional) |

For the ROMBA JTAG connector, you need a 6-pin, 2.54 mm pitch, dual sided board edge connector, such as [this one](https://www.digikey.es/en/products/detail/te-connectivity-amp-connectors/7-5530843-7/2310829). If you wire pin 6, **make sure you connect it to a 3.3 V source. Connecting it to 5V will destroy the ROMBA cartridge**. When in doubt, just do not wire this pin, then to flash the firmware, plug the ROMBA cart to the ROMBA programmer, so it stays powered during the process.

Once you have Lattice Diamond/Lattice Diamond Programmer software installed and your programmer ready, open the project file under `src/hdl/lromba/impl1/impl1.xcf` and proceed to write the firmware (`src/hdl/lromba/impl1/lromba_impl1.jed`). Done!

## ArchLinux specific details

If you are an ArchLinux (or derivative) user, I made a [lattice-diamond](https://aur.archlinux.org/packages/lattice-diamond) package you can install using your favorite AUR helper. I also wrote the [Lattice Diamond page in the ArchWiki](https://wiki.archlinux.org/title/Lattice_Diamond), you should read it. But if you are in a hurry, there are two things to take into account after installing Lattice Diamond:

1. You need a license. You can request a free one [here](https://www.latticesemi.com/Support/Licensing.aspx). Then, when you receive the license file, put it under `/usr/local/diamond/<version>/license/license.dat` (change `<version>` to the version number of the suite you are installing).
2. If programming with the FT2232H board fails, run as root `# rmmod ftdi_sio` and try again. Note that you will have to repeat this each time you plug the programmer (unless you prefer blacklisting the `ftdi_sio` module).
